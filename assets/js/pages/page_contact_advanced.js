var PageContactForm = function () {

    return {
        
        //Contact Form
        initPageContactForm: function () {
	        // Validation
	        $("#sky-form3").validate({
	            // Rules for form validation
	            ignore: "",
	            rules:
	            {
	                name:
	                {
	                    required: true
	                },
	                email:
	                {
	                    required: true,
	                    email: true
	                },
	                message:
	                {
	                    required: true,
	                    minlength: 10
	                },
	                subject:{
	                		required: true,
	                },
	                recaptcha: {
		           required: function() {
		               if(grecaptcha.getResponse() == '') {
		                   return true;
		               } else {
		                   return false;
		               }
		           }
		       }
	            },
	                                
	            // Messages for form validation
	            messages:
	            {
	                name:
	                {
	                    required: 'Por favor ingrese su nombre',
	                },
	                email:
	                {
	                    required: 'Por favor ingrese su email',
	                    email: 'Por favor ingrese un email VALIDO'
	                },
	                message:
	                {
	                    required: 'Por favor ingrese su mensaje'
	                },
	            
	                subject:
	                {	
	                		required: 'Por favor ingrese el asunto'
	                }, 
	                recaptcha:{
	                	required: 'Por favor valide que no es un robot'
	                },
	                
	            },
	                                
	            // Ajax form submition                  
	            submitHandler: function(form)
	            {
	                $(form).ajaxSubmit(
	                {
						url: url_post,
	                    beforeSend: function()
	                    {
	                        $('#sky-form3 button[type="submit"]').attr('disabled', true);
	                    },
	                    success: function()
	                    {

	                    	var result = arguments[0];
	                    	if(result[0])
	                        	$("#sky-form3").addClass('submited');
	                        else
	                           alert("Error del servidor, comuniquese al +57 031 300 31 13");
	                    }
	                });
	            },
	            
	            // Do not change code below
	            errorPlacement: function(error, element)
	            {
	                error.insertAfter(element.parent());
	            }
	        });
        }

    };
    
}();