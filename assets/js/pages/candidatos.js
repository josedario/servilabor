var ValidationCandidatos = function () {

    return {
        
        //Validation
        initValidation: function () {
	        $("#candidatos-form").validate({                   
	            // Rules for form validation
	            rules:
	            {
	                nombres:{
	                    required: true
	                },
	                documento:{
	                    required: true,
	                    digits: true,
	                },
	                correo : {
	                	required: true,
	                	email: true
	                },
	                direccion:{
	                		required: true,
	                },
	                ciudad:{
	                    required: true,
	                },
	                nacimiento:{
	                    required: true,
	                    date: true
	                },
	                telefono : {
	                		required: true,
	                		digits: true
	                },
	                interes : {
	                		required: true
	                },
	                hvplaceholder:{
	                		required: true
	                },
	                hv:{
	                	required: true
	                }
	                
	            },
	                                
	            // Messages for form validation
	            messages:
	            {
	                nombres: {
	                    required: 'Por favor ingrese su nombre'
	                },
	                documento:{
	                    required: 'Por favor ingrese su numero de documento',
	                    digits:   'Solo se permite el ingreso de numeros'
	                },
	                correo:{
	                    required: 'Por favor ingrese un correo electronico',
	                    email:    'Por favor ingrese correo electronico valido',
	                },
	                direccion:{
	                		required: 'Por favor ingrese dirección de residencia'
	                },
	                ciudad:{
	                    required: 'Por favor ingrese la ciudad en donde vive'
	                },
	                nacimiento:{
	                    required: 'Por favor indique su fecha de nacimiento',
	                    date :    'Ingrese una fecha de nacimiento valida',
	                },
	                telefono:
	                {
	                    required: 'Ingrese un numero de telefono para contacto',
	                    digits: 'Solo se permite el ingreso de numeros',
	                },
	                interes:{
	                		required: 'Seleccione un valor'
	                },
	                hvplaceholder:{
	                		required: 'Adjuntar hoja de vida'
	                },
	                hv:{
	                	required: ''
	                },
	                
	            },                  
	            
	            // Do not change code below
	            errorPlacement: function(error, element)
	            {
	                error.insertAfter(element.parent());
	            }
	        });
        }

    };
}();

var Masking = function () {

  return {
      //Masking
      initMasking: function () {
        $("#date1").mask("99/99/9999", {placeholder:"MM/DD/AAAA"});
      }

  };
  
}();