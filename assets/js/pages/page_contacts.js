var ContactPage = function () {

    return {
        
    	//Basic Map
        initMap: function () {
			var map;
			$(document).ready(function(){
			  map = new GMaps({
				div: '#map',
				scrollwheel: false,
				lat: 4.6628033,
				lng: -74.0584197
			  });
			  var marker = map.addMarker({
				lat: 4.6628033,
				lng: -74.0584197,
	            title: 'Servilabor, SAS.'
		       });
			});
        },

        //Panorama Map
        initPanorama: function () {
		    var panorama;
		    $(document).ready(function(){
		      panorama = GMaps.createPanorama({
		        el: '#panorama',
		        lat : 4.661045,
		        lng : -74.061990
		      });
		    });
		}        

    };
}();