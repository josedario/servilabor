var Tabla = function()
{
    this.$wrap  = $('<div class="tabla-wrap"></div>');
    this.$table = $('<table class="table table-bordered tabla"></table>');
    this.$thead = $('<thead><tr></tr></thead>');
    this.$tbody = $('<tbody></tbody>');
    this.$wrap.append(this.$table.append(this.$thead, this.$tbody));
    this.$trs = [];
    this.$tds = [];
    this.$paginacion = '';
    this.onPaginacionFns = [];
};

Tabla.prototype.fromJson = function(data)
{
    for(var i = 0; i < data.cols.length; i++)
        this.addTh(data.cols[i]);

    for(var i = 0; i < data.rows.length; i++)
        this.addRow(data.rows[i]);
    
    this.setPaginacion(data.paginacion);
};

Tabla.prototype.appendTo = function($cont)
{
    $cont.append(this.$wrap);
};
Tabla.prototype.setPaginacion = function(paginacion, callback)
{
    var self = this;
    this.$paginacion = $('<ul class="pagination pagination-sm no-margin pull-right"></ul>');
    for(var i = 0; i < paginacion.length; i++){
        var link = paginacion[i];
        var $li = $('<li class="'+link.liclass+'"></li>');
        var $link = $('<'+link.tag+' ' + (link.tag == 'a' ? 'href="'+link.href+'"' : '') + '>'+link.text+'</'+link.tag+'>');
        if(link.tag == 'a'){
            $link.click(function(e){
                e.preventDefault();
                var page = parseInt($(this).attr('href'));
                if(!isNaN(page))
                    self.onPaginacion(page);
            })
        }
        $li.append($link);
        this.$paginacion.append($li);
    }
    this.onPaginacion(callback);
    return this;
};
Tabla.prototype.appendPaginacionTo = function($cont)
{
    $cont.prepend(this.$paginacion);
};

Tabla.prototype.addTh = function(string)
{
    this.$thead.find('tr').append('<th>'+string+'</th>');
};
Tabla.prototype.addTd = function(row, html)
{
    while(typeof this.$trs[row] == "undefined"){
        var $tr = $('<tr></tr>');
        this.$trs.push($tr);
        this.$tbody.append($tr);
    }
    var $td = $('<td>'+html+'</td>');
    this.$trs[row].append($td);
    if(typeof this.$tds[row] == "undefined")
        this.$tds[row] = [];
    this.$tds[row].push($td);
};

Tabla.prototype.updateTd = function(row_index, td_index, string)
{
    var td = this.$tds[row_index][td_index];
    td.html(string);
};

Tabla.prototype.addRow = function(row)
{
    var $tr = $('<tr></tr>');
    for(var col in row)
        $tr.append('<td>'+row[col]+'</td>');
    this.$trs.push($tr);
    this.$tbody.append($tr);
};

Tabla.prototype.removeRows = function()
{
    this.$trs = [];
    this.$tds = [];
    this.$tbody.empty();
    return this;
};

Tabla.prototype.removePaginacion = function()
{
    if(this.$paginacion)
        this.$paginacion.remove();
    this.onPaginacionFns = [];
    return this;
};

Tabla.prototype.getRow = function(row_index)
{
    return this.$trs[row_index];
};

Tabla.prototype.onPaginacion = function(arg)
{
    if(typeof arg == "function")
        this.onPaginacionFns.push(arg);
    else
        for(var i = 0; i < this.onPaginacionFns.length; i++)
            this.onPaginacionFns[i](arg);
};