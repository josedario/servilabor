function renderButtonsEdit(data, type, row)
{
    var btns = {'ver' : 'eye-open'};
    var ret = '';
    var url = peek_url.replace('__id__', data);
    for(var key in btns)
        ret  += '<a class="btn btn-default btn-sm" href="'+url+'"'
            + '   data-toggle="tooltip" data-placement="top" title="'+key+'">'
            + '    <span class="glyphicon glyphicon-'+btns[key]+'" aria-hidden="true"></span>'
            + '</a>';
    return ret;
}


function dataTableConfig(){
    var columns = [];
    for(var i = 0; i < cols_info.length; i++) {
        var column_object = {};
        var col_info = cols_info[i]['dataTables'];

        for(var j in col_info)
            column_object[j] = col_info[j];

        if(col_info.data == "id")
            column_object['render'] = renderButtonsEdit;

        columns.push(column_object);
    }

    $('#table-datos').DataTable( {
        serverSide: true,
        ajax: {
            url  : ajax
        },
        pageLength: 100,
        columns : columns,
        //ordering: false,
        order : [[order_col, 'desc']],
        dom: "<'row'<'col-sm-6'f><'col-sm-6'p>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        language : {
            lengthMenu : "Mostrar _MENU_ usuarios por página",
            zeroRecords : "No hay usuarios para mostrar",
            info : "Mostrando usuarios de _START_ a _END_ de un total de _TOTAL_ usuarios",
            infoEmpty : "No hay usuarios para mostrar",
            infoFiltered : "(filtrado de _MAX_ usuarios en total)",
            paginate : {
                next : "Siguiente",
                previous : "Anterior"
            },
            search : "Buscar:"
        }
    });
}


$(function () {

    dataTableConfig();
});