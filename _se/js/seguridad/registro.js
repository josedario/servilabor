$(function(){
    $('#nacimiento').datepicker({
        format    : "yyyy-mm-dd",
        language  : "es",
        startDate : "1920-01-01",
        endDate   : "-14y",
        container :'.nacimiento-cont'
    });

});