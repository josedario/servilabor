$(function(){
    /*for(var i = 0; i < jsons.length; i++){
        var Node = new PrettyJSON.view.Node({
            el:$('#data-'+jsons[i].id),
            data: jsons[i].data
        });
        Node.collapseAll();
        new PrettyJSON.view.Node({
            el:$('#scrapper-'+jsons[i].id),
            data: jsons[i].scrapper
        }).collapseAll();
    }*/

    $('#modalVerProceso').on('show.bs.modal', function(e){
        var id = $(e.relatedTarget).data('pid');
        $.get(ajax.replace('__id__', id), function(data){
            $('#modalVerProceso .command textarea').append(data.process.command);

            var NodeA = new PrettyJSON.view.Node({
                el:$('#modalVerProceso .arguments'),
                data: data.process.args
            });
            NodeA.collapseAll();
            var NodeR = new PrettyJSON.view.Node({
                el:$('#modalVerProceso .respuesta'),
                data: data.process.respuesta
            });
            //NodeR.collapseAll();
            var NodeL = new PrettyJSON.view.Node({
                el:$('#modalVerProceso .log'),
                data: data.process.log
            });
            NodeL.collapseAll();
            $('#modalVerProceso .output textarea').append(data.process.output);
        })
    })
});