function vivienda_popular_ciudades($dpto, $ciudad)
{
    var dpto_id = $dpto.val();
    var ciudad_id = $ciudad.data('valor');
    $ciudad.find('option').remove();
    $.get(url_json_ciudad.replace('__did__', dpto_id), function(data, status){
        if(data.length > 0) {
            for (var j = 0; j < data.length; j++) {
                var selected = ciudad_id == data[j].id ? "selected" : "";
                $ciudad.append('<option value="'+data[j].id+'" '+selected+'>'+data[j].nombre+'</option>')
            }
            $ciudad.removeAttr('disabled');
            $ciudad.trigger("chosen:updated");
        }
    });
}
$(function(){
    var $modal = $('#modalTabla');
    $dpto = $modal.find('form [id="dpto_id"]');
    $ciudad = $modal.find('form [id="ciudad_id"]');

    $modal.on('hidden.bs.modal', function(){
        $ciudad.find('option').remove();
    });

    $('.tabla-editar').click(function() {
        vivienda_popular_ciudades($dpto, $ciudad);
    });

    if(tabla.modal_ver) {
        //vivienda_popular_ciudades($dpto, $ciudad);
    }
});