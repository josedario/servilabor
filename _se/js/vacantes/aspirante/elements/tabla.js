$(function(){
    var tabla = window.tabla;
    if(tabla){
        var $modal = $('#modalTabla');
        $modal.$title = $modal.find('.modal-title');

        var $modalBorrar = $('#modalTablaBorrar');
        $modalBorrar.$input = $modalBorrar.find('input[name=id]');
        $modalBorrar.$entidadIndex = $modalBorrar.find('input[name="entidad_index"]');

        tabla.limpiarModal = function()
        {
            $modal.find('.callout').remove();
            $modal.find('form input[type=text]').each(function(){
                $(this).val('');
            });
            $modal.find('form input[type=checkbox]').each(function(){
                $(this).prop('checked', false);
            });
            $modal.find('form select').each(function(){
                $(this).val(0);
            });
            $modal.find('.has-error').removeClass('has-error');
            $modal.find('.help-block.error').remove();
            $modal.find('input[name="id"]').val('');
        };

        $modal.on('hidden.bs.modal', function(){
            tabla.limpiarModal();
            $modal.$title.text(tabla.modal_titulo);
        });

        if(tabla.modal_ver)
            $modal.modal('show');

        $('.tabla-editar').click(function(){
            $btn = $(this);
            tabla.limpiarModal();
            var entidad = tabla.entidades[$btn.data('entidad-index')];
            for(var attr in entidad){
                $input = $modal.find('form [id="'+attr+'"]');
                if($input.length) {
                    if($input.attr('type') == 'checkbox') {
                        if(entidad[attr] == $input.val())
                            $input.prop('checked', true);
                    }else {
                        $input.val(entidad[attr]);
                        $input.data('valor', entidad[attr])
                    }
                }
                $modal.find('form input[name="entidad_index"]').val($btn.data('entidad-index'));
            }
            $modal.$title.text(tabla.modal_titulo.replace('Agregar', 'Editar'));
            $modal.modal('show');
        });
        $('.tabla-eliminar').click(function(){
            $btn = $(this);
            var entidad_index = $btn.data('entidad-index');
            var entidad = tabla.entidades[entidad_index];
            $modalBorrar.$input.val(entidad.id);
            $modalBorrar.$entidadIndex.val(entidad_index);
            $modalBorrar.modal('show');
        });
    }
});