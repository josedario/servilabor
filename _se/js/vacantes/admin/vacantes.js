function renderButtonsEdit(data, type, row)
{
    var despublicada = false;
    
    var btns = {'editar' : 'pencil', 'duplicar' : 'duplicate', 'borrar' : 'erase'};
    
    var ret = '';
    for(var key in btns) {
        var url = window["url_"+key].replace('__id__', data);
        ret += '<a class="btn btn-default btn-sm show-loading" href="'+url+'"'
            + '   data-toggle="tooltip" data-placement="top" title="' + key + '">'
            + '    <span class="glyphicon glyphicon-' + btns[key] + '" aria-hidden="true"></span>'
            + '</a>';
    }
    return ret;
}

function renderButtonsPortales(data, type, row)
{
    var access = user_portal_access;
    var ret = '';
    for(var i = 0; i < access.length; i++) {
        var cls = 'btn-default';
        var disabled = '';
        for(var j in data)
            if(data[j] == access[i]) {
                cls = 'btn-primary disabled';
                disabled = 'disabled';
            }

        ret += '<button '+disabled+' data-vid="'+row.id+'" data-p="'+access[i]+'" class="show-loading btn btn-sm ' + cls + ' portal-publish-button">'
            + '    <span class="icon-'+access[i]+'"></span>'
            + '</button>';

    }
    return ret;
}

function renderButtonsSociales(data, type, row)
{
    var redes = redes_sociales;
    var ret = '';
    for(var i in redes) {
        var disabled = '';
        var cls = ['btn-default', 'redes-publish-button btn btn-sm show-loading'];
        for (j in data)
            if (j == redes[i].id) {
                cls[0] = 'btn-primary disabled';
                disabled = 'disabled';
            }
        ret += '<button ' + disabled + ' data-vid="' + row.id + '" data-rid="' + redes[i].id + '" class="' + cls.join(' ') + '">'
            + '    <span class="fa fa-' + redes[i].name.toLowerCase() + '"></span>'
            + '</button>';
    }
    return ret;
}

function renderButtonAspirantes(data, type, row)
{
    var clss = 'btn-default';
    var disabled = 'disabled';
    if(data > 0){
        clss = 'btn-success';
        disabled = '';
    }
    var url = url_aspirantes.replace('__id__', row.id);
    var ret = '<a class="btn '+clss+' btn-sm show-loading" href="'+url+'" '+disabled+' data-toggle="tooltip" data-placement="top" title="aspirantes">'
        + '<i class="fa fa-users"></i>&nbsp;&nbsp;'
        + data
        + '</a>';
    return ret;
}

function renderButtonVigente(data, type, row)
{
    var icon = 'glyphicon glyphicon-eye-open';
    var color= 'btn-primary';
    var tooltip = 'despublicar';
    if(data == 0){
        icon = 'glyphicon glyphicon-eye-close';
        color = 'btn-default';
        tooltip = 'publicar';
    }
    return '<button class="btn btn-sm '+color+' '+tooltip+'" data-vid="'+row.id+'" data-toggle="tooltip" data-placement="top" title="' + tooltip + '">' +
        '<i class="' + icon + '"></i></button>';
}

function publishVacante(vid, p)
{
    var form = $("#publish-vacante");
    form.find("input[name='vid']").val(vid);
    form.find("input[name='p']").val(p);
    form.submit();
}

function publishRedes(vid, rid)
{
    var form = $("#publish-redes");
    form.find("input[name='vid']").val(vid);
    form.find("input[name='rid']").val(rid);
    form.submit();
}
function despublicar(vid)
{
    var form =$('#despublicar-vacante');
    form.find("input[name=vid]").val(vid);
    form.submit();
}
function publicar(vid)
{
    var form =$('#publicar-vacante');
    form.find("input[name=vid]").val(vid);
    form.submit();
}

function dataTableConfig(){
    var columns = [];
    for(var i = 0; i < cols_info.length; i++) {
        var column_object = {};
        var col_info = cols_info[i]['dataTables'];

        for(var j in col_info)
            column_object[j] = col_info[j];

        if(col_info.data == "id")
            column_object['render'] = renderButtonsEdit;
        else if(col_info.data == "portal")
            column_object['render'] = renderButtonsPortales;
        else if(col_info.data == "red_social")
            column_object['render'] = renderButtonsSociales;
        else if(col_info.data == 'usuario')
            column_object['render'] = renderButtonAspirantes;
        else if(col_info.data == 'vigente')
            column_object['render'] = renderButtonVigente;

        columns.push(column_object);
    }

    $('#table-vacantes').DataTable( {
        serverSide: true,
        ajax: {
            url : ajax_url,
            data : { user_admin : user_admin, user_id : user_id}
        },
        pageLength: 25,
        columns : columns,
        ordering: true,
        order : [[order_col, 'desc']],
        dom: "<'row'<'col-sm-6'l><'col-sm-6'p>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        language : {
            lengthMenu : "Mostrar _MENU_ vacantes por página",
            zeroRecords : "No hay vacantes para mostrar",
            info : "Mostrando vacantes de _START_ a _END_ de un total de _TOTAL_ vacantes",
            infoEmpty : "No hay vacantes para mostrar",
            infoFiltered : "(filtrado de _MAX_ vacantes en total)",
            paginate : {
                next : "Siguiente",
                previous : "Anterior"
            },
            search : "Buscar:"
        },
        initComplete: function(){
            //eventos de los selects para buscar en cada columna
            this.api().columns().every(function(){
                var column = this;
                var index = column[0][0];
                var sel = $(column.header()).closest('thead').first('tr').find('td:eq('+index+') select')
                    .on( 'change', function () {
                        column
                            .search($(this).val(), false, false )
                            .draw();
                    } );
            });

        },
        "drawCallback": function( settings ) {
            $(".portal-publish-button").click(function(){
                var data = $(this).data();
                publishVacante(data.vid,data.p);
            });
            $(".redes-publish-button").click(function(){
                var data = $(this).data();
                publishRedes(data.vid, data.rid);
            });
            $(".despublicar").click(function(){
                despublicar($(this).data('vid'));
            });
            $(".publicar").click(function(){
                publicar($(this).data('vid'));
            });
            //se activan tooltips de botones
            $('[data-toggle="tooltip"]').tooltip();
            //show-loadings
            $(".show-loading").click(function(e){
                $("#loading").show();
            });
        }
    });
    /*$('#table-vacantes').on( 'draw.dt', function () {
     console.log( 'Redraw occurred at: '+new Date().getTime() );
     } );*/
}

function modalExportConfig()
{

    $("#export-btn").click(function(){
        var data = {
            admn : user_admin,
            user : $('#export-form select.user').val(),
            date : $('#export-form select.date').val(),
            cols : []
        };
        $('#export-form input:checked').each(function(){
            data.cols.push($(this).attr("name"));
        });
        var form_data = new FormData();
        for(var i in data)
            form_data.append(i, data[i]);

        $.ajax({url: ajax_exportar, type:	'POST', data: form_data, cache: false, dataType:	'json',
            processData:	false,	//	Don't	process	the	files
            contentType:	false,	//	Set	content	type	to	false	as	jQuery	will	tell	the	server	its	a	query	string	request
            success:	function(data,	textStatus,	jqXHR){
                window.location.href = data.file;
                $("#exportModal").modal('hide');
            },
            error:	function(jqXHR,	textStatus,	errorThrown){
                var error = jqXHR.responseText;
                alert(error);
            }
        });
    });

    /*cambia filtro tabla, cambia filtro modal export
    $(".table-select").change(function(){
        var name = $(this).attr('name');
        $('#export-form select.'+name).val($(this).val());
    });*/
}
$(function () {
    dataTableConfig();
    modalExportConfig();
});