var source = null;
function sse(uid){
    if(source == null) {
        source = new EventSource(stream_url);

        source.addEventListener("estados", function (e) {
            console.log("data :  " + e.data);
            
            var data = JSON.parse(e.data);
            for(var uid in data){
                var estado = data[uid];
                if(estado > 0){
                    var color = estado == 1 ? 'btn-success' : 'btn-danger';
                    $('#upload-novasoft-'+uid).removeAttr('disabled')
                        .attr('class', 'btn '+color+' btn-sm show-loading upload_novasoft')
                        .html('<i class="fa fa-upload"></i>');
                }
            }
        });
        source.addEventListener("status", function (e) {
            var data = JSON.parse(e.data);
            console.log(data);
        });
        source.addEventListener("salida", function (e) {
            var data = JSON.parse(e.data);
            console.log("salida :  " + data.estado);
            source.close();
            source = null;
        });
    }
}

function uploadNovasoft($a)
{
    var url = $a.attr('href');
    var uid = $a.data('uid');
    $.get(url, function(data){
        if(data.exception)
            alert(data.exception);
        else{
            $a.html('<i class="fa fa-spinner fa-spin fa-1x fa-fw"></i> <span class="sr-only">Loading...</span>');
            sse(uid);
        }
        $("#loading").hide();
    });
}

function renderButtonsEdit(data, type, row)
{
    var btns = {'editar' : 'pencil'};
    var ret = '';
    for(var key in btns) {
        var url = window[key+"_url"].replace('__id__', data);
        ret += '<a class="btn btn-default btn-sm show-loading" href="'+url+'"'
            + '   data-toggle="tooltip" data-placement="top" title="' + key + '">'
            + '    <span class="glyphicon glyphicon-' + btns[key] + '" aria-hidden="true"></span>'
            + '</a>';
    }
    return ret;
}


function renderButtonComando(data, type, row)
{
    var url = novasoft_url.replace('__id__', row.id);

    var color = 'btn-default';
    if(data > 0)
        color = data == 1 ? 'btn-success' : 'btn-danger';
    var disabled = data != 0 ? '' : 'disabled';
    var ret = '<a '+disabled+' class="btn '+color+' btn-sm show-loading upload_novasoft" href="'+url+'" data-uid="'+row.id+'"'
            + '   data-toggle="tooltip" data-placement="top" title="Novasoft" id="upload-novasoft-'+row.id+'">';
    if(data == 0) {
        ret += '    <i class="fa fa-spinner fa-spin fa-1x fa-fw"></i> <span class="sr-only">Loading...</span>';
        sse(row.id);
    }else
        ret += '<i class="fa fa-upload"></i>';
    ret += '</a>';
    return ret;
}

function renderPorcentajeHv(data, type, row)
{
    var color = 'red';
    data > 50 ? color = 'yellow' : null;
    data >=100? color = 'green'  : null;
    return '<small class="label pull-right bg-'+color+'">'+data+'%</small>';
}
function renderPorcentajeMultiples(data, type, row)
{
    var color = 'red';
    if(data > 0)
        color = data <= 0 ? 'yellow' : 'green';
    return '<small class="label pull-right bg-'+color+'">'+data+'</small>';
}

function dataTableConfig(){
    var columns = [];
    var cols_porcentajes = ["hv", "estudios", "experiencia", "referencias", "redesSociales"];
    for(var i = 0; i < cols_info.length; i++) {
        var column_object = {};
        var col_info = cols_info[i]['dataTables'];

        for(var j in col_info)
            column_object[j] = col_info[j];

        if(col_info.data == "id")
            column_object['render'] = renderButtonsEdit;
        else if(col_info.data == "comando")
            column_object['render'] = renderButtonComando;
        if(cols_porcentajes.indexOf(col_info.data) > -1) {
            column_object["render"] = col_info.data == "hv" ? renderPorcentajeHv : renderPorcentajeMultiples;
        }

        columns.push(column_object);
    }
    var table = $('#table-aspirantes');
    
    table.DataTable( {
        serverSide: true,
        ajax: {
            url : ajax_url,
            data : function(d){
                return $.extend( {}, d, {
                    "estudio": $("#estudio").val(),
                    "experiencia": $("#experiencia").val()
                } );
            }
        },
        pageLength: 25,
        columns : columns,
        ordering: false,
        //order : [[order_col, 'desc']],
        dom: //"<'row'<'col-sm-6'f><'col-sm-6'p>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        language : {
            lengthMenu : "Mostrar _MENU_ aspirantes por página",
            zeroRecords : "No hay aspirantes para mostrar",
            info : "Mostrando aspirantes de _START_ a _END_ de un total de _TOTAL_ aspirantes",
            infoEmpty : "No hay aspirantes para mostrar",
            infoFiltered : "(filtrado de _MAX_ aspirantes en total)",
            paginate : {
                next : "Siguiente",
                previous : "Anterior"
            },
            search : "Buscar:"
        },
        "initComplete": function(){
            var self = this;
            //eventos de los selects para buscar en cada columna
            $('.col-filter').each(function(){
                var colindex = $(this).data('colindex');
                var $select = $(this).find('select');
                self.api().columns().every(function(){
                    var column = this;
                    var index = column[0][0];
                    if(index == colindex)
                        $select.on( 'change', function () {
                            column.search($(this).val(), false, false ).draw();
                        } );

                })
            });
            //input buscar
            $('#buscar').on( 'keyup', function () {
                table.DataTable().search( this.value ).draw();
            });
            //select estudio y experiencia
            $("#estudio, #experiencia").change(function(){
                table.DataTable().ajax.reload();
            });
        },
        "drawCallback": function(){
            //se activan tooltips de botones
            $('[data-toggle="tooltip"]').tooltip();

            //click en upload novasoft
            $('.upload_novasoft').click(function(e){
                e.preventDefault();
                var $this = $(this);
                if(!$this.prop('disabled')) {
                    $this.prop('disabled', true);
                    uploadNovasoft($this);
                }
            });
            $(".show-loading").click(function(e){
                $("#loading").show();
            });
        }
    });
}

$(function () {
    dataTableConfig();
    $('.select2').select2({
        //minimumResultsForSearch: Infinity
    });
});