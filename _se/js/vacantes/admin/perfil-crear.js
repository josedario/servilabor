var perfil_crear = {
    "$modal_regla" : "#modal-regla",
    "$cantidad_entidad" : "#cantidad-entidad",
    "$cantidad_entidad_clone" : false,
    "$cantidad_entidad_wrap" : '#cantidad-entidad-wrap',
    "$input_minimo" : '#cantidad-minimo',
    "$input_maximo" : '#cantidad-maximo',
    "$modal_regla_label" : '#modal-regla-label',
    "$modal_regla_submit" : '#modal-regla-submit',
    "$regla_update_table" : false,
    "entidades_selected" : [],
    "update" : false,
    "on_submit" : false,
    "$filtro_modal" : '#modal-filtro',
    "$filtro_modal_submit" : '#modal-filtro-submit',
    "$filtro_modal_label" : '#modal-filtro-label',
    '$input_filtro_campo_id' : '#filtro-campo-id',
    '$input_filtro_valor' : '#filtro-valor',
    'filtro_entidad_id' : false,
    '$filtro_tr_update' : false,
    'minimo_pregistro' : false,

};
/**
 * Checkbox global por entidad para seleccionar todos los campos de la misma
 */
function all_campos_checkbox()
{
    var $entidades = $('input[type="checkbox"].entidad');

    //organizamos campos por entidades
    var entidades = {};
    $entidades.each(function(){
        var entidad_nombre = $(this).attr('name');
        entidades[entidad_nombre] = [];
        $('input[type="checkbox"].campo').each(function(){
            if($(this).data('entidad') == entidad_nombre)
                entidades[entidad_nombre].push($(this));
        })
    });
    //evento: al cambiar checkbox de entidad se propaga en sus campos
    $entidades.each(function(){
        var $this = $(this);
        $this.change(function(){
            for(var i = 0; i < entidades[$this.attr('name')].length; i++) {
                var $input = entidades[$this.attr('name')][i];
                if(!$input.prop('disabled')) {
                    if ($this.is(':checked'))
                        !$input.is(':checked') ? $input.prop('checked', true) : null;
                    else
                        $input.is(':checked') ? $input.prop('checked', false) : null;
                }
            }
        })
    });
}


/**
 * Inicializa variables para reglas y filtros
 */
function vars_init()
{
    for(var i in perfil_crear)
        if(typeof perfil_crear[i] == 'string')
            perfil_crear[i] = $(perfil_crear[i]);
    //removemos el select de entidad para tenerlo como reserva de las entidades
    perfil_crear.$cantidad_entidad.detach();

    //obtenemos las entidades ya seleccionadas para reglas
    $('input.cantidad-entidad-selected').each(function(){
        perfil_crear.entidades_selected.push($(this).val());
    });

    perfil_crear.$input_minimo.change(function(){
        var $this = $(this);
        if(perfil_crear.minimo_pregistro != false && $this.val() < perfil_crear.minimo_pregistro){
            $this.val(perfil_crear.minimo_pregistro);
        }
    })

}

/**
 *  REGLAS -------------------------------------------------------------------------------------------------------------
 */
/**
 * Al dar editar, toma la información de la regla que sirve para llenar el modal
 * @param $relatedTarget
 * @returns {{entidad_id: number, minimo: number, maximo: number}}
 */
function regla_get_data($relatedTarget)
{
    var data = {'entidad_id' : 0, 'minimo' : 0, 'maximo' : 0};
    var $table = $relatedTarget.closest('table.reglas');
    perfil_crear.$regla_update_table = $table;
    for(var i in data)
        data[i] = $table.find('input[type=hidden].'+i).val();
    return data;
}

/**
 * Llamada antes de abrir modal de regla, prepara y limpia campos
 * @param entidad_id
 * @param minimo
 * @param maximo
 * @param pid
 */
function regla_modal_limpiar(entidad_id, minimo, maximo, pid)
{
    var entidades_selected = perfil_crear.entidades_selected.slice();
    minimo = typeof minimo == "undefined" ? '' : minimo;
    maximo = typeof maximo == "undefined" ? '' : maximo;
    entidad_id = typeof entidad_id == "undefined" ? false : entidad_id;
    pid = typeof pid == "undefined" ? false : pid;

    //si entidad_id venimos a editar, borramos la entidad para evitar duplicados
    if(entidad_id){
        perfil_crear.entidades_selected = [];
        for(var i = 0; i < entidades_selected.length; i++)
            if(entidades_selected[i] != entidad_id)
                perfil_crear.entidades_selected.push(entidades_selected[i]);
        entidades_selected = perfil_crear.entidades_selected.slice();
    }

    //Modificamos select de entidades para que muestre unicamente las que no tienen regla
    perfil_crear.$cantidad_entidad_wrap.empty();
    var $cantidad_entidad_clone = perfil_crear.$cantidad_entidad.clone();
    $cantidad_entidad_clone.empty();
    perfil_crear.$cantidad_entidad.find('option').each(function(){
        var $option = $(this).clone();
        if($option.val() == 0 || entidades_selected.indexOf($option.val()) == -1 || entidades_selected.indexOf(entidad_id) > -1) {
            $cantidad_entidad_clone.append($option);
        }
    });
    perfil_crear.$cantidad_entidad_wrap.append($cantidad_entidad_clone);
    perfil_crear.$cantidad_entidad_clone = $cantidad_entidad_clone;

    //limpiamos formulario
    perfil_crear.$input_minimo.val(minimo == 0 ? '' : minimo);
    perfil_crear.minimo_pregistro = pid == 1 ? minimo : false;

    perfil_crear.$input_maximo.val(maximo == 0 ? '' : maximo);
    if(entidad_id)
        $cantidad_entidad_clone.val(entidad_id);

    if(perfil_crear.update){
        perfil_crear.$modal_regla_label.text('Actualizar regla');
        perfil_crear.$modal_regla_submit.text('Actualizar');
    }else{
        perfil_crear.$modal_regla_label.text('Crear nueva regla');
        perfil_crear.$modal_regla_submit.text('Agregar');
    }
}
/**
 * Agrega nueva o actualiza regla, realiza validaciones y llama a la funcion pertinente para insertar HTML
 * @returns {boolean}
 */
function regla_agregar()
{
    var ok = true;
    var entidad_id = perfil_crear.$cantidad_entidad_clone.val();
    var minimo = perfil_crear.$input_minimo.val();
    var maximo = perfil_crear.$input_maximo.val();
    minimo = isNaN(minimo) ? 0 : minimo;
    maximo = isNaN(maximo) ? 0 : maximo;
    var label  = perfil_crear.$cantidad_entidad_clone.find('option:selected').text();
    //si selecciono entidad y algun valor para minimo o maximo, creamos la regla
    if(entidad_id != 0 && minimo >= 0 && maximo >= 0 && (minimo != 0 || maximo != 0) && (maximo == 0 || minimo <= maximo)) {
        if(perfil_crear.update)
            regla_agregar_update_html(entidad_id, label, minimo, maximo);
        else
            regla_agregar_insert_html(entidad_id, label, minimo, maximo);
        perfil_crear.entidades_selected.push(entidad_id);
    }else{
        //TODO agregar mensaje de validación
        ok = false;
    }
    return ok;
}
/**
 * Creación de html para una regla creada
 * @param entidad_id
 * @param entidad_nombre
 * @param minimo
 * @param maximo
 */
function regla_agregar_insert_html(entidad_id, entidad_nombre, minimo, maximo)
{
    var $reglas_wrap = $('.reglas-wrap');
    if($reglas_wrap.find('.no-hay-reglas').length > 0)
        $reglas_wrap.empty();

    $reglas_wrap.append(
        '<table class="table table-bordered reglas">'+
        '   <tr><th>Entidad</th> <th>Mínimo</th> <th>Máximo</th> ' +
        '   <th>' +
        '       <a class="btn btn-xs btn-success pull-right" data-toggle="modal" data-target="#modal-regla"> ' +
        '           <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Editar regla"></i> ' +
        '       </a> ' +
        '       <a class="btn btn-xs btn-success pull-right borrar-regla" data-toggle="modal" data-target="#modal-regla"> ' +
        '           <i class="fa fa-eraser" data-toggle="tooltip" data-placement="top" title="Borrar regla"></i> ' +
        '       </a>' +
        '   </th>' +
        '   </tr> ' +
        '   <tr> ' +
        '       <td class="td-label">'+entidad_nombre+'</td> ' +
        '       <td class="td-minimo">'+ (minimo == 0 ? '-' : minimo) + '</td>' +
        '       <td class="td-maximo">'+ (maximo == 0 ? '-' : maximo) + '</td> ' +
        '       <td>' +
        '           <input type="hidden" class="cantidad-entidad-selected entidad_id" name="cantidad['+entidad_id+'][perfil_hv_entidad_id]" value="'+entidad_id+'">' +
        '           <input type="hidden" class="minimo" name="cantidad['+entidad_id+'][minimo]" value="'+minimo+'">' +
        '           <input type="hidden" class="maximo" name="cantidad['+entidad_id+'][maximo]" value="'+maximo+'">' +
        '       </td> ' +
        '   </tr> ' +
        '   <tr> ' +
        '       <th colspan="4" class="bg-gray disabled">Filtros ' +
        '           <a class="btn btn-xs btn-success pull-right agregar-filtro" data-toggle="modal" data-target="#modal-filtro" >Agregar Filtro </a> ' +
        '       </th> ' +
        '   </tr> ' +
        '   <tr> ' +
        '       <td colspan="4" class="filtro-wrap">' +
        '           <table class="table table-bordered filtros"><tr class="no-hay-filtros"><td>No hay filtros</td></tr> </table> ' +
        '       </td> ' +
        '   </tr> ' +
        '</table>'
    );
}
/**
 * Modificacion del html de la regla que se acutalizo
 * @param entidad_id
 * @param entidad_nombre
 * @param minimo
 * @param maximo
 */
function regla_agregar_update_html(entidad_id, entidad_nombre, minimo, maximo)
{
    var $table = perfil_crear.$regla_update_table;
    var $input_entidad_id = $table.find('input.entidad_id');
    var $input_minimo = $table.find('input.minimo');
    var $input_maximo = $table.find('input.maximo');
    var entidad_cambio = $input_entidad_id.val() != entidad_id;
    $table.find('.td-label').text(entidad_nombre);
    $table.find('.td-minimo').text(minimo == 0 ? '-' : minimo);
    $table.find('.td-maximo').text(maximo == 0 ? '-' : maximo);
    $input_minimo.val(minimo);
    $input_maximo.val(maximo);
    if(entidad_cambio){
        var old_entidad_id = $input_entidad_id.val();
        $input_entidad_id.attr('name', $input_entidad_id.attr('name').replace(old_entidad_id, entidad_id));
        $input_minimo.attr('name', $input_minimo.attr('name').replace(old_entidad_id, entidad_id));
        $input_maximo.attr('name', $input_maximo.attr('name').replace(old_entidad_id, entidad_id));
        $table.find('td.filtro-wrap').empty().append('<table class="table table-bordered table-filtros"><tr><td>No hay filtros</td></tr> </table> ');
    }
    $input_entidad_id.val(entidad_id);
}

function borrar_regla($relatedTarget)
{
    var $table = $relatedTarget.closest('table.reglas');
    var entidad_id = $table.find('input.entidad_id').val();
    var tmp = [];
    for(var i = 0; i < perfil_crear.entidades_selected.length; i++)
        if(perfil_crear.entidades_selected[i] != entidad_id)
            tmp.push(perfil_crear.entidades_selected[i]);
    perfil_crear.entidades_selected = tmp.splice();
    $table.remove();
}

/**
 * Comportamiento CRUD de relgas
 */
function reglas()
{
    //mostrar modal, ya se para crear o editar una regla
    perfil_crear.$modal_regla.on('show.bs.modal', function (e) {
        var $relatedTarget = $(e.relatedTarget);
        var pid = $relatedTarget.data('pid');
        if($relatedTarget.hasClass('borrar-regla')){
            borrar_regla($relatedTarget);
            return false;
        }
        else if(!$relatedTarget.hasClass('agregar-regla')){
            var regla_data = regla_get_data($relatedTarget);
            perfil_crear.update = true;
            regla_modal_limpiar(regla_data.entidad_id, regla_data.minimo, regla_data.maximo, pid);
        }else{
            perfil_crear.update = false;
            regla_modal_limpiar();
        }
        return true;
    });
    //al cerrar modal
    perfil_crear.$modal_regla.on('hide.bs.modal', function(e){
        var ok = true;
        //si vamos a crear regla
        if(perfil_crear.on_submit){
            ok = regla_agregar();
        }
        //si se cancelo
        else{
            //como al actualizar se elimina la entidad selected, volvemos a agregar
            if(perfil_crear.update){
                var entidad_id = perfil_crear.$cantidad_entidad_clone.val();
                if(entidad_id)
                    perfil_crear.entidades_selected.push(entidad_id);
            }
        }
        perfil_crear.on_submit = false;
        return ok;
    });

    perfil_crear.$modal_regla_submit.click(function(){
        perfil_crear.on_submit = true;
        perfil_crear.$modal_regla.modal('hide');
    });
}


/**
 *  FILTROS ------------------------------------------------------------------------------------------------------------
 */
/**
 * LLena el select de valores segun campo haciendo llamado ajax
 * @param campo_id
 * @param value
 */
function filtros_fill_values(campo_id, value)
{
    $.get(ajax_url.replace('__eid__', 0).replace('__cid__', campo_id), function(data, status){
       perfil_crear.$input_filtro_valor.empty();
       perfil_crear.$input_filtro_valor.append('<option value="0">Seleccione...</option>');
       for (var id in data) {
           var selected = value && id == value ? 'selected' : '';
           perfil_crear.$input_filtro_valor.append('<option value="'+id+'" '+selected+'>'+data[id]+'</option>');
       }
    });
}
/**
 * Llena el select de campos haciendo llamado ajax
 * @param entidad_id
 * @param value
 */
function filtros_fill_campos(entidad_id, value)
{
    $.get(ajax_url.replace('__eid__', entidad_id).replace('__cid__', 0), function(data, status){
        if(data.length > 0) {
            perfil_crear.$input_filtro_campo_id.empty();
            perfil_crear.$input_filtro_valor.empty();
            perfil_crear.$input_filtro_campo_id.append('<option value="0">Seleccione...</option>');
            for (var j = 0; j < data.length; j++) {
                var selected = value && data[j].id == value ? 'selected' : '';
                perfil_crear.$input_filtro_campo_id.append('<option value="'+data[j].id+'" '+selected+'>'+data[j].label+'</option>');
            }
        }
    });
}

/**
 * Agrega nueva o actualiza filtro, realiza validaciones y llama a la funcion pertinente para insertar HTML
 * @returns {boolean}
 */
function filtro_agregar()
{
    var ok = true;
    var entidad_id = perfil_crear.filtro_entidad_id;
    var campo_id   = perfil_crear.$input_filtro_campo_id.val();
    var campo_label= perfil_crear.$input_filtro_campo_id.find('option:selected').text();
    var valor      = perfil_crear.$input_filtro_valor.val();
    var valor_label= perfil_crear.$input_filtro_valor.find('option:selected').text();
    if(entidad_id != 0 && campo_id != 0 && valor != 0) {
        if(perfil_crear.update)
            filtro_agregar_update_html(campo_id, valor, campo_label, valor_label);
        else
            filtro_agregar_insert_html(entidad_id, campo_id, valor, campo_label, valor_label);
    }else{
        //TODO agregar mensaje de validación
        ok = false;
    }
    return ok;
}

/**
 * Inserta HTML para un nuevo filtro
 * @param entidad_id
 * @param campo_id
 * @param valor
 * @param campo_label
 * @param valor_label
 */
function filtro_agregar_insert_html(entidad_id, campo_id, valor, campo_label, valor_label)
{
    var $filtros_table = $('input.entidad_id[value="'+entidad_id+'"]').closest('table.reglas').find('table.filtros');
    if($filtros_table.find('.no-hay-filtros').length > 0)
        $filtros_table.empty();
    var count = $filtros_table.find('tr').length;
    $filtros_table.append(
        '<tr> ' +
        '   <td class="campo-label"><span>'+campo_label+'</span>'+
        '       <input type="hidden" name="filtro['+entidad_id+']['+count+'][perfil_hv_campo_id]" class="perfil_hv_campo_id" value="'+campo_id+'"> ' +
        '   </td> ' +
        '   <td> = </td> ' +
        '   <td class="value"><span>'+valor_label+'</span>' +
        '       <input type="hidden" name="filtro['+entidad_id+']['+count+'][valor]" class="valor" value="'+valor+'"> ' +
        '   </td> ' +
        '   <td> ' +
            '   <a class="btn btn-xs btn-default pull-right" data-toggle="modal" data-target="#modal-filtro"> ' +
            '       <i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Editar filtro"></i> ' +
            '   </a> ' +
            '   <a class="btn btn-xs btn-default pull-right borrar-filtro" data-toggle="modal" data-target="#modal-filtro"> ' +
            '       <i class="fa fa-eraser" data-toggle="tooltip" data-placement="top" title="Borrar filtro"></i> ' +
            '   </a> ' +
        '   </td> ' +
        '</tr>'
    );
}
/**
 * Modificacion del html del filtro que se acutalizo
 * @param campo_id
 * @param valor
 * @param campo_label
 * @param valor_label
 */
function filtro_agregar_update_html(campo_id, valor, campo_label, valor_label)
{
    var $tr = perfil_crear.$filtro_tr_update;
    $tr.find('input.perfil_hv_campo_id').val(campo_id);
    $tr.find('input.valor').val(valor);
    $tr.find('.campo-label span').text(campo_label);
    $tr.find('.value span').text(valor_label);
}

function filtro_borrar($relatedTarget)
{
    $relatedTarget.closest('tr').remove();
}

function filtros()
{
    //al mostrar modal, populamos los selects haciendo llamados ajax
    perfil_crear.$filtro_modal.on('show.bs.modal', function(e){
        var $relatedTarget = $(e.relatedTarget);
        if($relatedTarget.hasClass('borrar-filtro')){
            filtro_borrar($relatedTarget);
            return false;
        }
        perfil_crear.update = !$relatedTarget.hasClass('agregar-filtro');
        if(perfil_crear.update)
            perfil_crear.$filtro_tr_update = $relatedTarget.closest('tr');
        //obtenemos entidad, para saber sus campos
        var entidad_id = $relatedTarget.closest('table.reglas').find('input.entidad_id').val();
        perfil_crear.filtro_entidad_id = entidad_id;
        var campo_id = $relatedTarget.hasClass('agregar-filtro') ? false : $relatedTarget.closest('tr').find('input.perfil_hv_campo_id').val();
        filtros_fill_campos(entidad_id, campo_id);
        var value =  $relatedTarget.hasClass('agregar-filtro') ? false : $relatedTarget.closest('tr').find('input.valor').val();
        if(value) {
            filtros_fill_values(campo_id, value);
            perfil_crear.$filtro_modal_label.text('Actualizar filtro');
        }else
            perfil_crear.$filtro_modal_label.text('Crear nuevo filtro');
    });

    perfil_crear.$input_filtro_campo_id.change(function(){
       filtros_fill_values($(this).val())
    });
    
    perfil_crear.$filtro_modal.on('hide.bs.modal', function(e){
        var ok = true;
        //si vamos a crear regla
        if(perfil_crear.on_submit){
            ok = filtro_agregar();
        }
        perfil_crear.on_submit = false;
        return ok;
    });
    
    perfil_crear.$filtro_modal_submit.click(function(){
        perfil_crear.on_submit = true;
        perfil_crear.$filtro_modal.modal('hide');
    })
}


$(function(){
    all_campos_checkbox();

    vars_init();

    reglas();

    filtros();
});