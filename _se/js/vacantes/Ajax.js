Ajax = function(){
	this.ajaxFile = ajaxFile;
	this.cache = false;
};


Ajax.prototype._ajaxCall = function (data, success, error){
	var self = this;
	$.ajax({
		url:	self.ajaxFile,
		type:	'POST',
		data:	data,
		cache:	self.cache,
		dataType:	'json',
		processData:	false,	//	Don't	process	the	files
		contentType:	false,	//	Set	content	type	to	false	as	jQuery	will	tell	the	server	its	a	query	string	request
		success:	function(data,	textStatus,	jqXHR){
			//ajax_data : los datos que se enviaron en primera instancia
			var ajax_data = data.ajax_data;
			delete data.ajax_data;
			if(typeof	data.error	===	'undefined'){
				success(data, ajax_data);
			}else if(typeof data.error != 'undefined'){
				error(data, ajax_data);
			}
		},
		error:	function(jqXHR,	textStatus,	errorThrown){
			var error = jqXHR.responseText;
			msj.add(error);
		}
	});
};

Ajax.prototype.phpCall = function(request_function, data, success, error){
	var form_data = new FormData();
	
	form_data.append('request_function', request_function);
	
	for(var i in data)
		form_data.append(i, data[i]);

	
	this._ajaxCall(form_data, success, error);
};
