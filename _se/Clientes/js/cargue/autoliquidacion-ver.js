$(function(){

    $('.chosen-select').chosen();

    for(var i = 0; i < clientes.length; i++){
        var cd = ClienteDOM.instance(clientes[i].id, clientes[i].label);
        cd.en('fillTabla', function () {
            autoliquidacionEjecucion.stream();
        });
        
    }

    //form buscar por cliente
    var $form_buscar  = $('#form-buscar');
    $form_buscar.submit(function(){
        var cliente_id = $('select[name="buscar-cliente"]').val();
        if(cliente_id > 0){
            $form_buscar.attr('action', $form_buscar.attr('action').replace('__cid__', cliente_id));
            return true;
        }else if(cliente_id == null){
            $form_buscar.attr('action', $form_buscar.attr('action').replace('/__cid__', ''));
            return true;
        }
        return false;
    });
    $('input[name="limpiar-buscar"]').click(function(){
        $('select[name="buscar-cliente"]').val(null);
    })
});