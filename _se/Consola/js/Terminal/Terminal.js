var Terminal = function(json){
    this.comando   = json.comando;
    this.streamUrl = json.streamUrl;
    this.$terminal = null;
    this.$status   = null;
    this.source    = null;
    this.sourceClosed = true;
    this.statusCounter = 0;
    this.statusMessage = false;
    this.debugger = false;
};

Terminal.prototype.render = function($padre)
{
    this.$terminal = $('<div class="terminal comando-'+this.comando.id+'"></div>');
    this.$status   = $('<div class="terminal-status"></div>');
    this.$exception= $('<div class="terminal-exception"></div>');
    $padre.append(this.$terminal, this.$status, this.$exception);
    this.stream();
};

Terminal.prototype.addStatus = function(message, type)
{
    if(!this.debugger) {
        this.$exception.empty();
        this.$status.empty();
        if(this.statusMessage == message){
            message += this.statusCounter ? " (" + this.statusCounter + ")" : "";
            this.statusCounter++;
        }else{
            this.statusMessage = message;
            this.statusCounter = 0;
        }
        this.$status.append('<span>'+message+"</span><br />");
    }else
        this.$status.append('<span>'+type + " : " + message +"</span><br />");
};
Terminal.prototype.addException = function(message, type)
{
    if(!this.debugger) {
        this.$status.empty();
        this.$exception.append('<span>'+message + "</span><br />");
    }else
        this.$status.append('<span>'+type + " : " + message +"</span><br />");
};

Terminal.prototype.stream = function()
{
    var self = this;

    var path = self.streamUrl;
    
    this.source = new EventSource(path);
    this.sourceClosed = false;
    this.statusCounter = 0;
    
    this.source.addEventListener("salida", function (e) {
        var data = JSON.parse(e.data);
        for(var i = 0; i < data.salida.length; i++){
            var salida = new ComandoSalida(data.salida[i]);
            self.$terminal.append(salida.getSalida());
            self.$terminal[0].scrollTop = self.$terminal[0].scrollHeight;
        }
    }, false);

    this.source.addEventListener("status", function (e) {
        var data = JSON.parse(e.data);
        self.addStatus(data.message, "status");
    });

    this.source.addEventListener("finSalida", function (e) {
        var data = JSON.parse(e.data);
        self.addStatus(data.message, "finSalida");
        self.streamClose();
    });

    this.source.addEventListener("comandoException", function (e) {
        var data = JSON.parse(e.data);
        var message = data.message;
        if(typeof(data.comandoException) !== "undefined")
            message = data.comandoException.message;
        self.addException(message, "comandoException");
        self.streamClose();
    });

    this.source.addEventListener("exception", function (e) {
        var data = JSON.parse(e.data);
        self.addException(data.message, "exception");
        self.streamClose();
    });

    this.source.onerror = function (e) {
        self.addException("SSE ERROR", "onerror");
        self.streamClose();
    };
};

Terminal.prototype.streamClose = function()
{
    if(!this.sourceClosed) {
        this.source.close();
        this.sourceClosed = true;
    }
};