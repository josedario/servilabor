function crear_opcion()
{
    var $opcion = $('.opcion-wrap:last');
    var opcion_num = Number.parseInt($opcion.find('label').text().replace('Opcion', '')) + 1;
    $opcion = $opcion.clone();
    $opcion.find('input').val('');
    return $opcion.find('label').text('Opcion ' + opcion_num).end();

}
$(function(){
    $('#agregar-opcion').click(function(){
        $('.opciones-wrap').append(crear_opcion());
    });
    
    var objetos = TerminalControlador.objetos;
    for(var i in objetos){
        objetos[i].render($('.terminal-wrap'));
    }
});