


<div class="container content profile">
	<div class="row">
		
		
<h4 style="text-align: center;"><b>REGLAMENTO INTERNO DE TRABAJO DE SERVILABOR S.A.S</b></h4>
<h4 style="text-align: center;"><b>CAPÍTULO I</b></h4>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 1°</strong> El presente es el Reglamento Interno de Trabajo prescrito por la empresa <strong>SERVILABOR EMPRESA DE</strong> <strong>SERVICIOS CORPORATIVOS S.A.S</strong>.,domiciliada en Bogotá, D. C. Calle 73 N°16-22, oficina 301 y puntos de trabajo; sus dispociones quedan sometidos tanto la Empresa como todos sus trabajadores. Este reglamento hace parte de los contratos individuales de trabajo celebrados o que se celebren con los trabajadores, salvo estipulaciones en contrario que sin embargo solo pueden ser favorables al trabajador.</span></p>
<p><span style="font-weight: 400;">La naturaleza jurídica de SERVILABOR S.A.S, es un contratista independiente y, por tanto y verdadero empleador y no representante, ni intermediario que contrata la ejecución de una o varias obras o la prestación de servicios, en beneficio de terceros  ́por un precio determinado, asuminedo todos los riesgos, para realziarlo con sus propios medios y con libertad y autonomía directiva.( art 34 CST)</span></p>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>CAPÍTULO II</b></h4>
<h4 style="text-align: center;"><b>CONDICIONES DE ADMISIÓN</b></h4>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 2<strong>°</strong></strong> Quien aspire a ser trabajador en la empresa <strong>SERVILABOR S.A.S.</strong> debe hacer la solicitud por escrito para ser registrarlo como aspirante, presentar hoja de vida actualizada</span></p>
<p><span style="font-weight: 400;">acompañada de los siguientes documentos:</span></p>
<ol style="list-style-type: lower-alpha;">
	<li><span style="font-weight: 400;">Documento de identidad, según sea el caso</span></li>
	<li><span style="font-weight: 400;">Certificación escrita del último empleador con quien haya trabajado en que conste el </span>tiempo de servicio, la índole de la labor ejecutada y salario.</li>
	<li><span style="font-weight: 400;">La Empresa ordenara al aspirante los exámenes médicos de ingreso que considere relevantes para la labor que habrá de desarrollar, los cuales se llevarán a cabo por el médico encargado por la empresa, siempre que estos no atenten contra su dignidad o intimidad de conformidad con los profesiogramas realizados para tal fin, el médico encargado revisará los certificados de salud y podrá exigir otros si los considera necesarios. La empresa pagará el costo de los exámenes exigidos y permitidos por las disposiciones legales vigentes.</span></li>
	<li><span style="font-weight: 400;">Certificación de estudios dependiendo el perfil y cargo a cubrir.</span></li>
	<li><span style="font-weight: 400;">Certificación de comprobación de derechos expedida por las entidades de seguridad social donde haya sido afiliado con anterioridad. Si no ha sido afiliado al sistema no se exigirá.</span></li>
	<li>Autorización escrita del Inspector de trabajo o, en su defecto, de la primera autoridad local, a solicitud de los padres y, a falta de estos, el Defensor de Familia, cuando el aspirante sea menor de dieciocho (18) años.</li>
</ol>
<p><span style="font-weight: 400;">Cumplidos los anteriores requisitos la Empresa no está obligada a admitir al aspirante, pero puede decidir si lo vincula o no mediante contrato de trabajo a término fijo, indefinido, por la duración de la obra o labor contratada, para labores accidentales, ocasionales o transitorias; o para reemplazar personal en vacaciones, en uso de licencia, para atender incrementos de producción.</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 1:</strong> El empleador podrá establecer en el Reglamento, además de los documentos mencionados, todos aquellos que considere necesarios para admitir o no admitir al aspirante. Sin embargo, tales exigencias no deben incluir documentos, certificaciones o datos prohibidos expresamente por las normas jurídicas para tal efecto: Así, es prohibida la exigencia de la inclusión de formatos o cartas de solicitud de empleo "datos acerca del estado civil de las personas número de hijos que tengan, la religión que profesan o el partido político al cual pertenezcan..." (art. primero, Ley 13 de 1972); lo mismo que la exigencia de la prueba de gravindex para las mujeres (art.43 C.N.; arts primero y segundo convenio No. 111 de la OIT., Resolución número 4050 de 1994 del Ministerio de Trabajo y Seguridad Social), el examen de SIDA (Articulo 22 decreto reglamentario 559 de 1991), ni la libreta militar (Articulo 111 decreto 2150 de 1995).</span></p>
<p><span style="font-weight: 400;">Cumplidos lo anteriores requisitos, la empresa no está obligada a admitir al aspirante.</span></p>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>CONTRATO DE APRENDIZAJE</b></h4>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 3°</strong>. Contrato de aprendizaje es una forma especial dentro del Derecho Laboral, mediante la cual una persona natural desarrolla formación teórica practica en una entidad autorizada, a cambio de que una empresa patrocinadora proporcione los medios para adquirir formación profesional metódica y completa requerida en el oficio, actividad u ocupación y esto le implique desempeñarse dentro del manejo administrativo , operativo comercial o financiero propósitos del giro ordinario de las actividades de la empresa. (Articulo 30 de la Ley 789 de 2002)</span></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 4 °</strong> El contrato de aprendizaje deberá de contener los siguientes requisitos:</span></p>
<ol style="list-style-type: lower-alpha;">
	<li><span style="font-weight: 400;">Nombre e identificación de las partes. Entidad patrocinadora, entidad formadora y </span>aprendiz, incluidos los de sus representantes legales cuando sea necesario.</li>
	<li>Clase de capacitación formación técnica o tecnológica en la parte teórica que recibe o recibirá el aprendiz.</li>
	<li>Área de producción, actividad u ocupación materia de la complementación práctica.</li>
	<li>Duración y fechas de los periodos correspondientes a las fases teóricas y prácticas. </li>
	<li>Monto del apoyo de sostenimiento mensual.</li>
	<li>Derechos y obligaciones de las partes.</li>
	<li>Firma de las partes y fecha en la que suscribe</li>
</ol>
<p><span style="font-weight: 400;"><strong>ARTICULO 5°</strong>: Durante toda la vigencia de la relación. El aprendiz recibirá de la empresa un apoyo de sostenimiento mensual que sea como mínimo en la fase lectiva equivalente al 50% de un (1) salario mínimo legal vigente. El apoyo de sostenimiento, durante la fase práctica será equivalente al setenta y cinco por ciento (75%) de un salario mínimo mensual legal vigente. (Articulo 30, literal d Ley 789 de 2002).</span></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 6°</strong>: En lo referente a la determinación del número de aprendices, la empresa se ceñirá a lo establecido en la Ley 789 de 2002, articulo 33, por el cual el Servicio Nacional de Aprendizaje SENA, será la encargada de la asignación de la cuota de aprendices, de acuerdo a la cantidad de trabajadores que se encuentren vinculados a la compañía. (Articulo 33 de la Ley 789 de 2002).</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 7°</strong>: Los obligados a cumplir la cuota de aprendizaje podrán en su defecto cancelar al SENA una cuota mensual reulstante de multiplicar el 5% del número total de trabajadores, excluyendo los trabajadores independientes o transitorios, por un salario mínimo legal vigente. En el caso que la monetización sea parcial esta será proporcional al número de aprendices que dejen de hacer la práctica para cumplir la cuota mínima obligatoria. (Articulo 34 de la Ley 789 de 2002)</span></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 8°</strong>: La duración del contrato de aprendizaje no podrá ser superior a dos años. El contrato de aprendizaje celebrado a término mayor del señalado se considera, para todos los efectos legales, regido por las normas del Código Sustantivo de Trabajo, en el lapso que exceda a la correspondiente duración del contrato de aprendizaje. (Articulo 30 de la Ley 789 de 2002).</span></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 9°</strong>: Cuando el contrato de aprendizaje termine por cualquier causa, la empresa deberá reemplazar al aprendiz o aprendices para conservar la proporción que le haya sido señalada por el Servicio Nacional de Aprendizaje. (Parágrafo, articulo 33 Ley 789 de 2002).</span></p>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>PERIODO DE PRUEBA</b></h4>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 10°:</strong> Una vez admitido el aspirante, se podrá estipular en el contrato de trabajo que suscriban las partes, un periodo de prueba, que tendrá como objeto apreciar por parte de la empresa, las aptitudes del trabajador y por parte de éste, las conveniencias de las condiciones de trabajo. (Artículo 76 C.S.T).</span></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 11°</strong>: El período de prueba deber ser estipulado por escrito y en caso contrario los servicios se entienden regulados por las normas generales del contrato de trabajo (Artículo 77, numeral 1o C.S.T).</span></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 12°</strong>: El período de prueba no puede exceder de dos (2) meses. En los </span><span style="font-weight: 400;"></span><span style="font-weight: 400;">contratos de trabajo a término fijo, cuya duración sea inferior a un (1) año, el período de prueba no podrá ser superior a la quinta parte del término inicialmente pactado para el respectivo contrato, sin que pueda exceder de dos meses. Cuando entre un mismo empleador y trabajador se celebren contratos de trabajo sucesivos, no es válida la estipulación del período de prueba, salvo para el primer contrato (Artículo 7 Ley 50 de 1990).</span></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 13°</strong>: Durante el período de prueba, el contrato puede darse por terminado unilateralmente en cualquier momento y sin previo aviso, pero si expirado el período de prueba y el trabajador continuare al servicio del empleador, con consentimiento expreso o tácito, por ese solo hecho, los servicios prestados por aquel a éste, se considerarán regulados por las normas del contrato de trabajo desde la iniciación de dicho período de prueba. (Artículo 80 C.S.T)</span></p>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>CAPÍTULO III</b></h4>
<h4 style="text-align: center;"><b>TRABAJADORES ACCIDENTALES O TRANSITORIOS</b></h4>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 14°</strong>: Son meros trabajadores accidentales o transitorios, los que se ocupen en labores de corta duración no mayor de un mes y de índole distinta a las actividades normales de la empresa. Estos trabajadores tienen derecho, además del salario, al descanso remunerado en dominicales y festivos (Artículo 6- C.S.T).</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>CAPÍTULO IV</b></h4>
<h4 style="text-align: center;"><b>HORARIO DE TRABAJO</b></h4>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 15°</strong>: Las horas de entrada y salida de los trabajadores, son las que a continuación se expresan así:</span></p>
<p><b>Empleados Administrativos:</b></p>
<p><span style="font-weight: 400;">Lunes a Viernes</span></p>
<p><span style="font-weight: 400;">Entrada en la Mañana: <br />
 7:00 a.m – 12:30 m</span></p>
<p><span style="font-weight: 400;">Horario de Alimentación: 12:30 m a 1:30 pm.</span></p>
<p><span style="font-weight: 400;">Entrada en la Tardes: <br />
 2:00 p.m a 5:00 p.m <br />
 Hora de Salida: 5:00 pm</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 1</strong>: El horario de trabajo será el establecido por el servicio del cual la empresa outsorcing se encuentra prestando a la empresa cliente; Sin superar la jornada máxima legal.</span></p>
<p><span style="font-weight: 400;"></span><b>Los días laborables son: Lunes, Martes, Miércoles, Jueves, Vierns, sábado.</b></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 2</strong>: Cuando la empresa tenga más de cincuenta (50) trabajadores que laboren cuarenta y ocho (48) horas a la semana, éstos tendrán derecho a que dos (2) horas de dicha jornada, por cuenta del empleador, se dediquen exclusivamente a actividades recreativas, culturales, deportivas o de capacitación (Ley 50 de 1990, artículo 21,).</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 16°: JORNADA ESPECIAL</strong>.- El empleador y el trabajador pueden acordar temporal o indefinidamente la organización de turnos de trabajo sucesivos, que permitan operar a la empresa o secciones de la misma sin solución de continuidad durante todos los días de la semana, siempre y cuando el respectivo turno no exceda de seis (6) horas al día y treinta y seis (36) a la semana.</span></p>
<p><span style="font-weight: 400;">En este caso no habrá lugar al recargo nocturno ni al previsto para el trabajo dominical o festivo, pero el trabajador devengará el salario correspondiente a la jornada ordinaria de trabajo, respetando siempre el mínimo legal o convencional y tendrá derecho a un (1) día de descanso remunerado.</span></p>
<p><span style="font-weight: 400;">El empleador no podrá, aún con el consentimiento del trabajador, contratarlo para la ejecución de dos (2) turnos en el mismo día, salvo en labores de supervisión, dirección, confianza o manejo. (Artículo 20, literal c, Ley 50 de 1990).</span></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 17°: JORNADA FLEXIBLE</strong>: El empleador y el trabajador podrán acordar que la jornada semanal de cuarenta y ocho (48) horas se realice mediante jornadas diarias flexibles de trabajo, distribuidas en máximo seis días a la semana con un día de descanso obligatorio, que podrá coincidir con el domingo. En este, el número de horas de trabajo diario podrá ser de mínimo cuatro (4) horas continuas y hasta diez (10) horas diarias sin lugar a ningún recargo por trabajo suplementario, cuando el número de horas de trabajo no exceda el promedio de cuarenta y ocho (48) horas semanales dentro de la jornada ordinaria de 6:00 am a 10 p.m. (Articulo 51 de la Ley 789 de 2002)</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 18°: TRABAJO POR TURNOS</strong>: Cuando la naturaleza de la labor no exija actividad continua y se lleve a cabo por turnos de trabajadores, la duración de la jornada puede ampliarse en más de ocho (8) horas actividad o en mas de (48) horas semanales, siempre que el promedio de las horas de trabajo calculando para un período que no exceda de (3) tres semanas, no pase de ocho (8) horas diarias de cuarenta y ocho (48) a la semana. Esta amplaición no constiutye trabajo suplementario o horas extras. (Articulo 165 C.S.T)</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>CAPÍTULO V</b></h4>
<h4 style="text-align: center;"><b>LAS HORAS EXTRAS Y TRABAJO NOCTURNO</b></h4>
<p><b>ARTÍCULO 19°: TRABAJO ORDINARIO Y TRABAJO NOCTURNO.</b></p>
<ol>
	<li><b></b><span style="font-weight: 400;">Trabajo ordinario es el que se realiza entre las seis horas (6:00 a.m) y las veintidós horas (10:00 p.m).</span></li>
	<li>Trabajo Nocturno es el comprendido entre las veintidós horas (10:00 pm) y las seis horas (6:00 a.m) (Ley 789 de 2002, articulo 25)</li>
</ol>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 20</strong>°: El trabajo suplementario o de horas extras, es el que excede de la jornada ordinaria y en todo caso el que excede la máxima legal (articulo 159 C.S.T).</span></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 2</strong>1°: Las horas extras de trabajo, diurnas o nocturnas, no podrán exceder de (2) horas diarias y doce (12) semanales.</span></p>
<p><span style="font-weight: 400;">Cuando la jornada de trabajo se ample por acuerdo entre empleadores y trabajadores a diez (10) horas diarias, no pódran en el mismo día laborar horas extras. (Articulo 22 de la Ley 50 de 1990)</span></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 22°</strong>: Tasas y liquidación de recargos.</span></p>
<ol>
	<li><span style="line-height: 1.5;">El trabajo nocturno, por el solo hecho de ser nocturno, se remunera con un recargo del treinta y cinco (35%) sobre el valor del trabajo diurno, con excepción del caso de la jornada de treinta y seis (36) horas semanales prevista en el artículo 20 literal c de la Ley 50 de 1990.</span></li>
	<li><span style="font-weight: 400;"> El trabajo extra diurno se remunera con un recargo del veinticinco por ciento (25%) </span>sobre el valor del trabajo ordinario diurno.</li>
	<li><span style="font-weight: 400;">El trabajo extra nocturno se remunera con un recargo del setenta y cinco por ciento </span>(75%) sobre el valor del trabajo ordinario diurno,</li>
	<li><span style="font-weight: 400;">Cada uno de los recargos antedichos se produce de manera exclusiva, es decir, sin </span>acumularlo con algún otro (Articulo 24, Ley 50 de 1990).</li>
</ol>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 1:</strong> El pago del trabajo suplementario o de horas extras y de recargo por trabajo nocturno, en su caso, se efectuará junto con el salario del período en que se han causado, o a más tardar con el salario del periodo siguiente (artículo 134, ordinal 2o, C.S.T.).</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 2:</strong> La empresa podrá implementar turnos especiales de trabajo nocturno, de acuerdo con lo previsto por el Decreto 2352 de 1965.</span></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 23°</strong>: La empresa no reconocerá trabajo suplementario o de horas extras, sino cuando expresamente lo autorice a sus trabajadores, de acuerdo con lo establecido para tal efecto en el artículo 21 de este Reglamento.</span></p>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>CAPÍTULO VI</b></h4>
<h4 style="text-align: center;"><b>DÍAS DE DESCANSO LEGALMENTE OBLIGATORIOS</b></h4>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 24°</strong>: Serán de descanso obligatorio remunerado, los domingos y días de fiesta que sean reconocidos como tales en nuestra legislación laboral.</span></p>
<ol>
	<li><span style="font-weight: 400;">Todo trabajador tiene derecho al descanso remunerado en los siguientes días de fiesta de carácter civil o religioso: 1o y 6 de enero, 19 de marzo, 1o de mayo, 29 de junio, 20 de julio, 7 de agosto, l5 de agosto, 12 de octubre, 1o y 11 de noviembre, 8 y 25 de diciembre, además de los días jueves y viernes santo, Ascensión del Señor, Corpus Christi y Sagrado Corazón de Jesús.</span></li>
	<li><span style="font-weight: 400;"> Pero el descanso remunerado del seis de enero, diecinueve de marzo, veintinueve de junio, quince de agosto, doce de octubre, primero y once de noviembre, Ascensión del Señor, Corpus Christi y Sagrado Corazón de Jesús, cuando no caigan en día lunes se trasladarán al lunes siguiente a dicho día. Cuando las mencionadas festividades caigan en domingo, el descanso remunerado, igualmente se trasladará al lunes.</span></li>
	<li><span style="font-weight: 400;"> Las prestaciones y derechos que para el trabajador originen el trabajo en los días festivos, se reconocerán con relación al día de descanso remunerado establecido en el inciso anterior. (Ley 51 de 1983).</span></li>
</ol>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 1.</strong> Cuando la jornada de trabajo convenida por las partes, en días u horas, no implique la prestación de servicios en todos los días laborables de la semana, el trabajador tendrá derecho a la remuneración del descanso dominical en proporción al tiempo laborado (artículo 26, numeral 5o Ley 50 de 1990).</span></p>
<p><b>ARTICULO 25°: TRABAJO DOMINICAL Y FESTIVO SE REMUNERA</b></p>
<ol>
	<li><span style="font-weight: 400;"> El trabajo en domingo y festivos se remunerará con un recargo del setenta y cinco por ciento (75%) sobre el salario ordinario en proporción a las horas laboradas.</span></li>
	<li><span style="font-weight: 400;"> Si con el domingo coincide otro día de descanso remunerado solo tendrá derecho el trabajador, si trabaja, al recargo establecido en el numeral anterior(Articulo 26 de la Ley 789 de 2002)</span></li>
	<li><span style="font-weight: 400;"> Se exceptúa el caso de la jornada de treinta y seis (36) horas semanales previstas en el artículo 20 literal c) de la Ley 50 de 1990.</span></li>
</ol>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 1:</strong> El trabajador podrá convenir con el empleador su día de descanso obligatorio el día sábado o domingo, que será reconocido en todos sus aspectos como descanso dominical obligatorio institucionalizado. </span><span style="font-weight: 400;">Interprétese la expresión dominical contenida en el régimen laboral en este sentido exclusivamente para el efecto del descanso obligatorio. (Articulo 26 de Ley 789 de 2002)</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 2</strong>: Se entiende que el trabajo dominical es ocasional cuando el trabajador labora hasta dos (2) domingos durante el mes calendario. Se entiende que el trabajo dominical es habitual cuando el trabajador labore tres (3) o más domingos durante el mes calendario.(Articulo 26, parágrafo Ley 789 de 2002). </span></p>
<p><span style="font-weight: 400;"><strong>PARAGRAFO 3</strong>: <strong>AVISO SOBRE TRABAJO DOMINICAL</strong>. Cuando se tratare de trabajos habituales o permanentes en domingo, el empleador debe fijar en lugar público del establecimiento, con anticipación de doce (12) horas por lo menos, la relación del personal de trabajadores que por razones del servicio no puede disponer del descanso dominical. En esta relación se incluirán también el día y las horas de descanso compensatorio (artículo185, C.S.T.).</span></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 26°:</strong> El descanso en los días domingos y los demás días expresados en el artículo 22 de este reglamento, tiene una duración mínima de 24 horas, salvo la excepción consagrada en el literal c) del artículo 20 de la Ley 50 de 1990 (artículo 25, Ley 50 de 1990).</span></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 27°:</strong> Cuando por motivo de fiesta no determinada en la Ley 51 de 1983, la empresa suspendiere el trabajo, está obligada a pagarlo como si se hubiere realizado. No está obligada a pagarlo cuando hubiere mediado convenio expreso para la suspensión o compensación o estuviere prevista en el reglamento, pacto, convención colectiva o fallo arbitral. Este trabajo compensatorio se remunerará sin que se entienda como trabajo suplementario o de horas extras. (Articulo 178 del C.S.T).</span></p>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>VACACIONES REMUNERADAS</b></h4>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 28°</strong>: Los trabajadores que hubieren prestado sus servicios durante un (1) año tienen derecho a quince (15) días hábiles consecutivos de vacaciones remuneradas (artículo 186, numeral 1o, C.S.T.).</span></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 29°</strong>: La época de vacaciones debe ser señalada por el empleador a más tardar dentro del año siguiente, y ellas deben ser concedidas oficiosamente o a petición del trabajador, sin perjudicar el servicio y la efectividad del descanso. El empleador tiene que dar a conocer al trabajador con quince (15) días de anticipación la fecha en que le concederá las vacaciones.</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO</strong>: Todo empleador debe de llevar un registro especial de vacaciones en el que anotará la fecha en que ha ingresado al establecimiento cada trabjador, la fecha en que toma sus vacaciones anuales y en que las termina y la remuneración recibia por las mismas. (Articulo 187 del C.S.T.)</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 30°</strong>: Si se presenta interrupción justificada en el disfrute de las vacaciones, el trabajador no pierde el derecho a reanudarlas (artículo 188, C.S.T.).</span></p>
<p><span style="font-weight: 400;"></span><span style="font-weight: 400;"><strong>ARTÍCULO 31°</strong>: Empleador y Trabajador, podrán acordar por escrito previa solicitud del trabajador, que se pague hasta la mitad de las vacaciones.</span></p>
<p><span style="font-weight: 400;">Para la compensación en dinero de las vacaciones se tomará como base el último salario devengado por el trabajador. (Artículo 20 Ley 1429 de 2010).</span></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 32° ACUMULACIÓN:</strong> En todo caso, el trabajador gozará anualmente, por lo menos de seis (6) días hábiles continuos de vacaciones, los que no son acumulables.Las partes pueden convenir en acumular los días restantes de vacaciones hasta por dos años. La acumulación puede ser hasta por cuatro (4) años, cuando se trate de trabajadores técnicos, especializados, de confianza, de manejo o de extranjeros que presten sus servicios en lugares distintos a los de la residencia de sus familiares (artículo 190, C.S.T.).</span></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 33° REMUNERACIÓN</strong>: Durante el período de vacaciones el trabajador recibirá el salario ordinario que esté devengando el día que comience a disfrutar de ellas. En consecuencia, sólo se excluirán para la liquidación de las vacaciones el valor del trabajo en días de descanso obligatorio y el valor del trabajo suplementario o de horas extras. Cuando el salario sea variable, las vacaciones se liquidarán con el promedio de lo devengado por el trabajador en el año inmediatamente anterior a la fecha en que se concedan.(Articulo 6 del Decreto 13 de 1967)</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO</strong>. En los contratos a término fijo inferior a un (1) año, los trabajadores tendrán derecho al pago de vacaciones en proporción al tiempo laborado cualquiera que éste sea (artículo 3o, parágrafo, Ley 50 de 1990).</span></p>
<p>&nbsp;</p>
<h4 style="text-align: center;"> <b>PERMISOS</b></h4>
<p><span style="font-weight: 400;"><strong>ARTICULO 34°: PERMISOS Y LICENCIAS OBLIGATORIOS</strong>: La empresa concederá a sus trabajadores los permisos necesarios para el ejercicio del derecho al sufragio y para el desempeño de cargos oficiales transitorios de forzosa aceptación.</span></p>
<p><span style="font-weight: 400;">En caso de grave calamidad doméstica debidamente comprobada hasta por 1 día hábil, por mes calendario, el cual podrá ser prorrogado por la empresa, de conformidad con la sentencia C-930 de 2009, para concurrir en su caso al servicio médico correspondiente, para desempeñar comisiones sindicales inherentes a la organización y para asistir al entierro de sus compañeros, siempre que avisen con la debida oportunidad a la empresa y a sus representantes y que en los dos últimos casos, el número de los que se ausenten no sea tal, que perjudiquen el funcionamiento de la empresa.</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 1</strong>: Para los efectos del presente reglamento, se entienden por Calamidad Doméstica los siguientes hechos: La hospitalización del cónyuge o del compañero(a) permanente, hijos y padres del trabajador en el caso de que estos últimos convivan bajo el mismo techo con éste; los desastres naturales y los hechos catastróficos, fortuitos o imprevistos que afecten la salud, los bienes o la vivienda del trabajador y su grupo familiar.</span></p>
<p><strong></strong><span style="font-weight: 400;"><strong>PARÁGRAFO 2</strong>: La concesión de los permisos antes dichos estará sujeta a las siguientes condiciones:</span></p>
<ul style="list-style-type: circle;">
	<li><span style="font-weight: 400;">En caso de grave calamidad doméstica, la oportunidad del aviso puede ser anterior o posterior al hecho que lo constituye o al tiempo de ocurrir éste, según lo permita las circunstancias.</span></li>
	<li>En caso de entierro de compañeros de trabajo, el aviso puede ser hasta con un día de anticipación y el permiso se concederá hasta el 10% de los trabajadores.</li>
	<li>En los demás casos (sufragio, desempeño de cargos transitorios de forzosa aceptación y concurrencia al servicio médico correspondiente) el aviso se dará con la anticipación que las circunstancias lo permitan.</li>
	<li>Salvo convención en contrario y a excepción del caso de concurrencia al servicio médico correspondiente, el tiempo empleado en estos permisos puede descontarse al trabajador o compensarse con tiempo igual de trabajo efectivo en horas distintas a su jornada ordinaria, a opción de la empresa (numeral sexto, artículo 57, C.S.T.).</li>
</ul>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 3</strong>. En caso de permiso para asistir al servicio médico correspondiente, salvo los casos de urgencia, la solicitud se hará al jefe inmediato con mínimo dos (2) días de anticipación y éste deberá dar aviso a la empresa. Para efecto de sustentar este permiso, el trabajador deberá presentar una constancia de asistencia al servicio expedida por el médico o enfermera autorizados, en la que se indique la hora de inicio y terminación de la consulta médica. En caso de incapacidad motivada en accidente o enfermedad común o laboral, el trabajador deberá dar aviso al Jefe Inmediato y posteriormente allegará a la empresa, la incapacidad respectiva, bien sea personalmente o si por la gravedad de la enfermedad o el accidente no se puede movilizar, podrá hacerla llegar por medio de un familiar, amigo o compañero de trabajo o por cualquier otro medio tecnológico.</span></p>
<p><span style="font-weight: 400;">En el evento de grave calamidad doméstica debidamente comprobada, el trabajador debe informar de inmediato el hecho que la constituye, según lo permitan las circunstancias, y acreditarlo a través de algún medio probatorio, salvo que se trate de hechos suficientemente conocidos.</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 4.</strong> Cuando se trate de ejercer el derecho del sufragio; desempeño de cargos oficiales transitorios de forzosa aceptación: grave calamidad doméstica debidamente comprobada; desempeño de comisiones sindicales inherentes a la organización a que pertenezcan; asistencia al entierro de sus compañeros de trabajo; de la Licencia por Luto de que trata la ley 1280 de enero 5 de 2009; de la licencia de paternidad (sentencias C – 663 de 2009 y C – 174 de 2009 de la Corte Constitucional); de la Licencia de maternidad, o de la incapacidad, remunerada, el tiempo empleado por el trabajador no podrá descontársele del salario, ni pedírsele su compensación con tiempo igual de trabajo efectivo en horas distintas de su jornada ordinaria.</span></p>
<p><strong></strong><span style="font-weight: 400;"><strong>PARÁGRAFO 5.</strong> La empresa concederá a sus trabajadores la correspondiente licencia remunerada por luto, establecida en la Ley 1280 de 2009, a razón de 5 días hábiles, los cuales podrán ser suspendidos por incapacidad y otra licencia.</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 6:</strong> La empresa reconocerá a sus trabajadores, la licencia de ocho (8) días hábiles contenida en la Ley 1468 de 2011. Así mismo toda trabajadora en estado de embarazo tendrá derecho a catorce (14) semanas en la época de parto remunerada con el salario que devengue al entrar a disfrutar del descanso (Ley 1468 de 2011).</span></p>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>CAPÍTULO VII</b></h4>
<p><b>SALARIO MÍNIMO, CONVENCIONAL, LUGAR, DÍAS, HORAS DE PAGO Y PERIODOS QUE LO REGULAN</b></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 35°:</strong> Formas y libertad de estipulación.</span></p>
<ol>
	<li><span style="font-weight: 400;"> El empleador y el trabajador pueden convenir libremente el salario en sus diversas modalidades como por unidad de tiempo, por obra, o a destajo y por tarea, etc., pero siempre respetando el salario mínimo legal o el fijado en los pactos, convenciones colectivas y fallos arbitrales.</span></li>
	<li><span style="font-weight: 400;"> No obstante lo dispuesto en los artículos 13,14, 16,21, y 340 del Código Sustantivo del Trabajo y las normas concordantes con estas, cuando el trabajador devengue un salario ordinario superior a diez (10) salarios mínimos legales mensuales, valdrá la estipulación escrita de un salario que además de retribuir el trabajo ordinario, compense de antemano el valor de prestaciones, recargos y beneficios tales como el correspondiente al trabajo nocturno, extraordinario o al dominical y festivo, el de primas legales, extra legales, las cesantías y sus intereses, subsidios y suministros en especie; y, en general, las que se incluyan en dicha estipulación, excepto las vacaciones.</span></li>
	<li><span style="font-weight: 400;"> En ningún caso el salario integral podrá ser inferior al monto de diez (10) salarios mínimos legales mensuales, más el factor prestacional correspondiente a la empresa que no podrá ser inferior al treinta por ciento (30%) de dicha cuantía.</span></li>
	<li><span style="font-weight: 400;"> Este salario no estará exento de las cotizaciones a la seguridad social, ni de los aportes al SENA, ICBF y Cajas de Compensación Familiar, pero la base para efectuar los aportes parafiscales es el setenta por ciento (70%).</span></li>
	<li><span style="font-weight: 400;"> El trabajador que desee acogerse a esta estipulación, recibirá la liquidación definitiva de su auxilio de cesantía y demás prestaciones sociales causadas hasta </span>esa fecha, sin que por ello se entienda terminado su contrato de trabajo (artículo 18, Ley 50 de 1.990).</li>
</ol>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 36°:</strong> Se denomina jornal el salario estipulado por días y, sueldo el estipulado por período mayores (artículo 133, C.S.T.).</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 37°:</strong> Salvo convenio por escrito, el pago de los salarios se efectuará en el lugar en donde el trabajador presta sus servicios durante el trabajo, o inmediatamente después que éste cese. (Artículo 138, numeral primero, C.S.T.).</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 1:</strong> Periodos de Pago: Los pagos se efectuarán así: Los jornales al vencimiento de cada semana, y los sueldos al personal de planta se cancelaran de forma mensual los días 20 de cada mes en el caso de que este día sea festivo, el pago se trasladará al día hábil siguiente y a los trabajadores en misión conforme lo establezca la empresa usuaria. El pago se hará mediante abono en cuenta corriente o de ahorros de cada trabajador, si este la posee, previo acuerdo entre éste y la Compañía. La Empresa, puede utilizar medios electrónicos para la notificación del pago de la nómina a sus trabajadores, y se entiende que el acuse de recibo de ésta constituye un comprobante de que se efectuó el pago.</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 2:</strong> El pago del trabajo suplementario o de horas extras y el recargo por trabajo nocturno debe efectuarse junto con el salario ordinario del período en que se han causado o a más tardar con el salario del período siguiente. Todos estos pagos se deben hacer en las oficinas de la Empresa o a través del pago por el medio electrónico</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 38<strong>°:</strong></strong> El salario se pagará al trabajador directamente o a la persona que él autorice por escrito.</span></p>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>CAPÍTULO VIII</b></h4>
<h4 style="text-align: center;"><b>SERVICIOS MÉDICOS, MEDIDAS DE SEGURIDAD, RIESGOS PROFESIONALES, PRIMEROS AUXILIOS EN CASO DE ACCIDENTES DE TRABAJO, NORMAS SOBRE LABORES EN ORDEN A LA MAYOR HIGIENE, REGULARIDAD Y SEGURIDAD EN EL TRABAJO.</b></h4>
<p><span style="font-weight: 400;"><strong>ARTICULO 39°</strong> Es obligación del empleador velar por la salud, seguridad e higiene de los trabajadores a su cargo. Igualmente, es su obligación garantizar los recursos necesarios para implementar y ejecutar actividades permanentes en medicina preventiva y del trabajo, y en higiene y seguridad industrial, de conformidad al Sistema de Gestión en Seguridad y Salud en el trabajo, y con el objeto de velar por la protección integral del trabajador.</span></p>
<p><strong></strong><span style="font-weight: 400;"><strong>ARTICULO 40°</strong> Los servicios médicos que requieran los trabajadores se prestarán por las Empresas prestadoras de Salud (EPS) o Administradoras de Riesgos Laborales (A.R.L), a través de la Institución Prestadora de Servicios de Salud (I.P.S), a la cual estén afiliados. En caso de no afiliación estarán a cargo del empleador, sin perjuicio de las acciones legales pertinentes.</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 41° AVISO A LA EMPRESA SOBRE LA ENFERMEDAD</strong>: Todo trabajador, desde el mismo día en que se sienta enfermo, deberá comunicarlo al empleador, a su representante, o a quien haga sus veces, el cual hará lo necesario para que sea examinado por el médico correspondiente, a fin de que certifique si puede continuar o no en el trabajo, y en su caso determine la incapacidad y el tratamiento a que el trabajador debe someterse.</span></p>
<p><span style="font-weight: 400;">Si el trabajador, no diere aviso dentro del término indicado, o no se sometiere al examen médico que se haya ordenado, su inasistencia al trabajo se tendrá como injustificada para los efectos a que haya lugar, a menos que demuestre que estuvo en absoluta imposibilidad para dar el aviso y someterse al examen en la oportunidad debida.</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 42° INSTRUCCIONES Y TRATAMIENTOS MÉDICOS</strong>: Los trabajadores deben someterse a las instrucciones y tratamiento que ordena el médico que los haya examinado, así como a los exámenes y tratamientos preventivos que para todos o algunos de ellos ordena la empresa en determinados casos. El trabajador que sin justa causa se negare a someterse a los exámenes, instrucciones o tratamientos antes indicados, perderá el derecho a la prestación en dinero por la incapacidad que sobrevenga a consecuencia de esa negativa. (Articulo 55 Decreto 1295 de 1994).</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 1</strong>: El grave incumplimiento por parte del trabajador de las instrucciones reglamentos y determinaciones de prevención de riesgos, adoptados en forma general o especifica, y que se encuentren dentro de los Sistemas de Gestión de la Seguridad y Salud en el trabajo, de las respectivas empresas, que le hayan comunicado por escrito, facultan al empleador para la terminación del vinculo o relación laboral por justa causa.</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 43° PRIMEROS AUXILIOS</strong>: En caso de accidente de trabajo, el Jefe de la respectiva dependencia, o su representante, ordenará inmediatamente la prestación de los primeros auxilios, la remisión al medico y tomara todas las medidas que se consideren necesarias y suficientes para reducir al mínimo, las consecuencias del accidente, informando el mismo en los términos establecidos en el Decreto 1295 de 1994 ante la EPS y la ARL.</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 44° RESPONSABILIDAD DE LA EMPRESA</strong>: La empresa no será responsable por ningún accidente que haya sido provocado deliberadamente o por culpa del trabajador, en este caso, solo estará obligada a prestar los primeros auxilios. Tampoco responderá de la agravación que se presente en las lesiones o perturbaciones causadas por cualquier accidente, por razón de no haber dado el trabajador, el aviso oportuno correspondiente o haberlo demorado sin justa causa.</span></p>
<p><span style="font-weight: 400;"></span><span style="font-weight: 400;">En caso de accidente no mortal, aún el más leve incidente, el trabajador lo comunicará inmediatamente al empleador, a su representante, o a quien haga sus veces, para que prevean la asistencia médica y tratamiento según las disposiciones legales vigentes, el médico tratante correspondiente continuará el tratamiento respectivo e indicará las consecuencias del accidente y la fecha en que cese la incapacidad.</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 45° MEDIDAS DE HIGIENE OBLIGATORIAS:</strong> Los trabajadores deberán someterse a todas las medidas de higiene y seguridad que prescriban las autoridades del ramo en general, y en particular a las que ordene la empresa para prevención de las enfermedades y de los riesgos en el manejo de las máquinas y demás elementos de trabajo especialmente para evitar los accidentes de trabajo y enfermedades laborales.</span></p>
<p><span style="font-weight: 400;"><strong>PARAGRAFO 1:</strong> El incumplimiento por parte del trabajador de las instrucciones, reglamentos y determinaciones de prevención de riesgos, adoptados en forma general o específica y que se encuentren dentro del Programa de Seguridad y Salud en el trabajo de la respectiva empresa, que le hayan comunicado por escrito, facultan al empleador para la terminación del vínculo o relación laboral por justa causa, tanto para los trabajadores privados como los servidores públicos, previa autorización del Ministerio de Trabajo, respetando el derecho de defensa. (Artículo 91 Decreto 1295 de 1994)</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 46°</strong> Todas las empresas y las entidades administradoras de riesgos laborales deberán llevar estadísticas de los accidentes de trabajo y de las enfermedades laborales, para lo cual deberán, en cada caso, determinar la gravedad y la frecuencia de los accidentes de trabajo o de las enfermedades laborales, de conformidad con el reglamento que se expida. El Ministerio del Trabajo, establecerá las reglas a las cuales debe sujetarse el procesamiento y remisión de esta información (Artículo 61, decreto 1295 de 1994)</span></p>
<p><span style="font-weight: 400;">Todo accidente de trabajo o enfermedad laboral que ocurra en una empresa o actividad económica, deberá ser informado por el empleador a la entidad administradora de riesgos laborales y a la entidad promotora de salud, en forma simultánea, dentro de los dos días hábiles siguientes de ocurrido el accidente o diagnosticada la enfermedad.</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 1:</strong> De todo accidente se llevara registro en libro especial, con indicación de la fecha, hora, sector y circunstancias en que ocurrió, nombre de los testigos presénciales si los hubiere, y un relato sucinto de lo que puedan declarar.</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 47°</strong> En todo caso, en lo referente a los puntos de que trata este capítulo, tanto la empresa como los trabajadores, se someterán a las normas de riesgos laborales del Código Sustantivo del Trabajo, la Resolución No. 1016 de 1.989, expedida por el Ministerio de Trabajo y S. S. y las demás que con tal fin se establezcan. De la misma manera, ambas partes están obligadas a sujetarse al Decreto Ley 1295 de 1994, y la Ley 776 del 17 de diciembre de 2002, y Ley 1562 de 2012 del Sistema General de Riesgos Laborales, de conformidad a los términos estipulados en los preceptos legales pertinentes y demás normas concordantes y reglamentarias antes mencionadas.</span></p>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>CAPÍTULO IX</b></h4>
<h4 style="text-align: center;"><b></b><b>ORDEN JERARQUICO</b></h4>
<p><span style="font-weight: 400;"><strong>ARTICULO 48°</strong> El orden Jerárquico de acuerdo con los cargos existentes en la empresa, es el siguiente: GERENCIA GENERAL, SUBGERENCIA Y DIRECCION ADMINISTRATIVA, JEFATURAS DE ÁREAS Y COORDINACIONES.</span></p>
<p><span style="font-weight: 400;">Es necesario mencionar que para las agencias esta facultad estaré en cabeza del Jefe de Agencia respectivo.</span></p>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>CAPITULO X</b></h4>
<h4 style="text-align: center;"><b>PRESCRIPCIONES DE ORDEN</b></h4>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 49° </strong>:Los trabajadores tienen como deberes los siguientes:</span></p>
<ol style="list-style-type: lower-alpha;">
	<li><span style="font-weight: 400;">Respeto y subordinación a los superiores;</span></li>
	<li>Respeto a sus compañeros de trabajo;</li>
	<li>Procurar completa armonía con sus superiores y compañeros de trabajo en las relaciones personales y en la ejecución de labores;</li>
	<li>Guardar buena conducta en todo sentido y obrar con espíritu de leal colaboración en el orden moral y disciplina general de la empresa;</li>
	<li>Ejecutar los trabajos que le confíen con honradez, buena voluntad y de la mejor manera posible.</li>
	<li>Hacer las observaciones, reclamos y solicitudes a que haya lugar por conducto del respectivo superior y de manera fundada, comedida y respetuosa.</li>
	<li>Ser verídico en todo caso.</li>
	<li>Recibir y aceptar las órdenes, instrucciones y correcciones dadas por la Empresa o el usuario, relacionadas con el trabajo, el orden y la conducta en general, con su verdadera intención, que es en todo caso la de encaminar y perfeccionar los esfuerzos en provecho propio y de la empresa en general.</li>
	<li>Observar rigurosamente las medidas y precauciones que le indique su respectivo Jefe para el manejo de las máquinas o instrumentos de trabajo y</li>
	<li>Permanecer durante la jornada de trabajo en el sitio o lugar en donde debe desempeñar sus labores, siendo prohibido, salvo orden superior, pasar al puesto de trabajo de otros compañeros o ausentarse de él.</li>
</ol>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO</strong> Los directores o trabajadores no pueden ser agentes de la autoridad pública en los establecimientos o lugares de trabajo, ni intervenir en la selección del personal de la Policía, ni darle órdenes, ni suministrarle alojamiento o alimentación gratuitos, ni hacerle dádivas (artículo126, C.S.T.).</span></p>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>CAPÍTULO XI</b></h4>
<h4 style="text-align: center;"><b>LABORES PROHIBIDAS PARA MUJERES Y MENORES DE 18 AÑOS</b></h4>
<p><span style="font-weight: 400;"><strong>ARTICULO 50°</strong>. Queda prohibido emplear a los menores de dieciocho (18) años y a las mujeres en trabajo de pintura industrial, que entrañen el empleo de la cerusa, del sulfato de plomo o de cualquier otro producto que contenga dichos pigmentos. Las mujeres sin distinción de edad y los menores de dieciocho (18) años no pueden ser empleadas en trabajos subterráneos de las minas ni en general trabajar en labores peligrosas, insalubres o que requieran grandes esfuerzos (ordinales 2 y 3 del artículo 242 del C.S.T.)</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 51°</strong>- Los menores no podrán ser empleados en los trabajos que a continuación se enumeran, por cuanto suponen exposición severa a riesgos para su salud o integridad física:</span></p>
<ol>
	<li><span style="font-weight: 400;"> Trabajos que tengan que ver con sustancias tóxicas o nocivas para la salud.</span></li>
	<li><span style="font-weight: 400;"> Trabajos a temperaturas anormales o en ambientes contaminados o con </span>insuficiente ventilación.</li>
</ol>
<ol start="3">
	<li><span style="font-weight: 400;"> Trabajos subterráneos de minería de toda índole y en los que confluyen agentes nocivos, tales como contaminantes, desequilibrios térmicos, deficiencia </span>de oxígeno a consecuencia de la oxidación o la gasificación.</li>
</ol>
<ol start="4">
	<li><span style="font-weight: 400;"> Trabajos donde el menor de edad está expuestos a ruidos que sobrepasen </span>ochenta (80) decibeles.</li>
</ol>
<ol start="5">
	<li><span style="font-weight: 400;"> Trabajos donde se tenga que manipular con sustancias radioactivas, pinturas luminiscentes, rayos X, o que impliquen exposición a radiaciones ultravioletas, infrarrojas y emisiones de radio frecuencia.</span></li>
	<li>Todo tipo de labores que impliquen exposición a corrientes eléctricas de alto voltaje.</li>
</ol>
<ol start="7">
	<li><span style="font-weight: 400;"> Trabajos submarinos.</span></li>
	<li><span style="font-weight: 400;"> Trabajo en basurero o en cualquier otro tipo de actividades donde se generen </span>agentes biológicos patógenos.</li>
</ol>
<ol start="9">
	<li><span style="font-weight: 400;"> Actividades que impliquen el manejo de sustancias explosivas, inflamables o </span>cáusticas.</li>
</ol>
<ol start="10">
	<li><span style="font-weight: 400;"> Trabajos en pañoleros o fogoneros, en los buques de transporte marítimo.</span></li>
	<li><span style="font-weight: 400;"> Trabajos en pintura industrial que entrañen el empleo de la cerusa, de sulfato </span>de plomo o de cualquier otro producto que contenga dichos elementos.</li>
	<li>Trabajos en máquinas esmeriladoras, afilado de herramientas, en muelas abrasivas de alta velocidad y en ocupaciones similares.</li>
	<li>Trabajos en altos hornos, horno de fundición de metales, fábrica de acero, talleres de laminación, trabajos de forja, y prensa pesada de metales.</li>
</ol>
<ol start="14">
	<li><span style="font-weight: 400;"> Trabajos y operaciones que involucren la manipulación de cargas pesadas.</span></li>
	<li><span style="font-weight: 400;"> Trabajos relacionados con cambios, de correas de transmisión, aceite, engrasado y </span>otros trabajos próximos a transmisiones pesadas o de alta velocidad.</li>
	<li>Trabajos en cizalladoras, cortadoras, laminadoras, tornos, fresadoras, troqueladoras, otras máquinas particularmente peligrosas.</li>
</ol>
<ol start="17">
	<li><span style="font-weight: 400;"> Trabajos de vidrio y alfarería, trituración y mezclado de materia prima; trabajo de hornos, pulido y esmerilado en seco de vidriería, operaciones de limpieza por chorro de arena, trabajo en locales de vidriado y grabado, trabajos en la industria cerámica.</span></li>
	<li><span style="font-weight: 400;"> Trabajo de soldadura de gas y arco, corte con oxígeno en tanques o lugares </span>confinados, en andamios o en molduras precalentadas.</li>
	<li>Trabajos en fábricas en ladrillos, tubos y similares, moldeado de ladrillos a mano, trabajo en las prensas y hornos de ladrillos.</li>
	<li>Trabajo en aquellas operaciones y/o procesos en donde se presenten altas temperaturas y humedad.</li>
	<li>Trabajo en la industria metalúrgica de hierro y demás metales, en las operaciones y/o procesos donde se desprenden vapores o polvos tóxicos y en plantas de cemento.</li>
</ol>
<ol start="22">
	<li><span style="font-weight: 400;"> Actividades agrícolas o agro industriales que impliquen alto riesgo para la salud.</span></li>
	<li><span style="font-weight: 400;"> Las demás que señalen en forma específica los reglamentos del Ministerio de la </span>Protección Social.</li>
</ol>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 1</strong>. Los trabajadores menores de dieciocho (18) años y mayores de catorce (14), que cursen estudios técnicos en el Servicio Nacional de Aprendizaje o en un instituto técnico especializado reconocido por el Ministerio de Educación Nacional o en una institución del Sistema Nacional de Bienestar Familiar autorizada para el efecto por el Ministerio del Trabajo, (Art. 47 Decreto 205 de 2003), o que obtenga el certificado de aptitud profesional expedido por el Servicio Nacional de Aprendizaje, "SENA", podrán ser empleados en aquellas operaciones, ocupaciones o procedimientos señalados en este artículo, que a juicio del Ministerio del Trabajo, pueden ser desempeñados sin grave riesgo para la salud o la integridad física del menor mediante un adecuado entrenamiento y la aplicación de las medidas de seguridad que garanticen plenamente la prevención de los riesgos anotados. Quedan prohibidos a los trabajadores menores de dieciocho (18) años todo trabajo que afecte su moralidad. En especial les está prohibido el trabajo en casas de lenocinio y demás lugares de diversión donde se consuma bebidas alcohólicas. De igual modo se prohibe su contratación para la reproducción de escenas pornográficas, muertes violentas, apología del delito u otros semejantes. (Artículo 117- Ley 1098 de noviembre 8 de 2006- Código de la Infancia y la Adolescencia- Resolución No. 4448 de diciembre 2 del 2005.</span></p>
<p><span style="font-weight: 400;">Queda prohibido el trabajo nocturno para los trabajadores menores, no obstante los mayores de dieciséis (16) años y menores de dieciocho (18) años podrán ser autorizados para trabajar hasta las ocho (8) de la noche siempre que no se afecte su asistencia regular en un centro docente, ni implique perjuicio para su salud física o moral (artículo 243 del decreto 2737 de 1989).</span></p>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>CAPÍTULO XII</b></h4>
<h4 style="text-align: center;"><b>OBLIGACIONES ESPECIALES PARA EL EMPLEADOR Y LOS TRABAJADORES</b></h4>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 52°:</strong> Son obligaciones especiales del empleador:</span></p>
<ol>
	<li><span style="font-weight: 400;"> Poner a disposición de los trabajadores, salvo estipulación en contrario, los instrumernos adecuados y las materias primas necesarias para la realización de las labores.</span></li>
	<li><span style="font-weight: 400;"> Procurar a los trabajadores, locales apropiados y elementos adecuados de protección contra accidentes y enfermedades profesionales en forma que se </span><span style="font-weight: 400;"></span><span style="font-weight: 400;">garanticen razonablemente la seguridad y la salud.</span></li>
</ol>
<ol start="3">
	<li><span style="font-weight: 400;"> Prestar de inmediato los primeros auxilios en caso de accidentes o enfermedad. Para este efecto, el establecimiento mantendrá lo necesario según reglamentación de las autoridades sanitarias.</span></li>
	<li><span style="font-weight: 400;"> Pagar la remuneración pactada en las condiciones, períodos y lugares convenidos.</span></li>
	<li><span style="font-weight: 400;"> Guardar absoluto respeto a la dignidad personal del trabajador, sus creencias y </span>sentimientos.</li>
</ol>
<ol start="6">
	<li><span style="font-weight: 400;"> Conceder al trabajador las licencias necesarias para los fines y en los términos </span>indicados en el artículo 32 de este reglamento.</li>
</ol>
<ol start="7">
	<li><span style="font-weight: 400;"> Dar al trabajador que lo solicite, a la expiración del contrato, una certificación en que conste el tiempo de servicio, índole de la labor y salario devengado, e igualmente si el trabajador lo solicita, hacerle practicar examen sanitario y darle certificación sobre el particular, si al ingreso o durante la permanencia en el trabajo hubiere sido sometido a examen médico. Se considera que el trabajador por su culpa elude, dificulta o dilata el examen, cuando transcurridos cinco (5) días a partir de su retiro no se presenta donde el médico respectivo para las prácticas del examen, a pesar de haber recibido la orden correspondiente.</span></li>
	<li><span style="font-weight: 400;"> Pagar al trabajador los gastos razonables de venida y regreso, si para prestar su servicio lo hizo cambiar de residencia, salvo si la terminación del contrato se origina por culpa o voluntad del trabajador.</span></li>
</ol>
<p><span style="font-weight: 400;">Si el trabajador prefiere radicarse en otro lugar, el empleador le debe costear su traslado hasta concurrencia de los gastos que demandaría su regreso al lugar donde residía anteriormente.</span></p>
<p><span style="font-weight: 400;">En los gastos de traslado del trabajador, se entienden comprendidos los de familiares que con él convivieren.</span></p>
<ol start="9">
	<li><span style="line-height: 1.5;">Cumplir con el Reglamento y mantener el orden y la moralidad y el respeti a las leyes.</span></li>
	<li><span style="font-weight: 400;"> Conceder al trabajador en caso de fallecimiento de cónyugue, compañero o compañera permanente o de un familoiat hasta el segundo grado de consaguinidad, primero de afanidad y primero civil, una licencia remunerada por luto de 05 días hábiles, cualquiera sea sus modalidad de contratación o vinculación laboral.</span></li>
	<li><span style="font-weight: 400;"> Abrir y llevar al día los registros de horas extras y de trabajadores menores que </span>ordena la ley.</li>
	<li>Conceder en forma oportuna a la trabajadora en estado de embarazo, la licencia remunerada consagrada en el numeral 1 del artículo 236 de forma tal que empiece a disfutarla de forma tal que empiece a disfrutarla de manera obligatoria una (1) semana antes o (2) semanas antes de la fecha probable del parto según decisión de la futura madre conforme al certificado medico a que se refiere numeral 3 del citado articulo 236.</li>
	<li><span style="font-weight: 400;">Conceder a las trabajadoras que estén en período de lactancia los descansos </span>ordenados por el artículo 238 del Código Sustantivo del Trabajo.</li>
	<li><span style="font-weight: 400;">Conservar el puesto a las empleadas que estén disfrutando de los descansos remunerados, a que se refiere el numeral anterior, o por licencia de enfermedad motivada en el embarazo o parto. No producirá efecto alguno el despido que el empleador comunique a la trabajadora en tales períodos o que si acude a un preaviso, éste expire durante los descansos o licencias mencionadas.</span></li>
	<li><span style="font-weight: 400;"> Llevar un registro de inscripción de todas las personas menores de edad que </span>emplee, con indicación de la fecha de nacimiento de las mismas.</li>
	<li>Cumplir este reglamento y mantener el orden, la moralidad y el respeto a las leyes.</li>
	<li>Además de las obligaciones especiales a cargo del empleador, éste garantizará el acceso del trabajador menor de edad a la capacitación laboral y concederá licencia no remunerada cuando la actividad escolar así lo requiera. Será también obligación de su parte, afiliarlos a los regímenes de Seguridad Social a todos los trabajadores menores de edad que laboren a su servicio, lo mismo que suministrarles cada cuatro (4) meses en forma gratuita, un par de zapatos y un vestido de labor, teniendo en cuenta que la remuneración mensual sea hasta dos veces el salario mínimo vigente en la empresa (art, 57, C.S.T.).</li>
	<li><span style="line-height: 1.5;">Suministrar cada cuatro (4) meses en forma gratuita, un par de zapatos y un vestido de labor, teniendo en cuenta que la remuneración mensual sea hasta dos veces el salario mínimo vigente en la empresa (artículo 57, C.S.T.).</span></li>
	<li><span style="line-height: 1.5;">Propiciar un buen ambiente laboral, fomentar los mecanismos para prevenir situaciones de acoso laboral, así como apoyar las actividades y acuerdos emitidos por el comité de convivencia laboral.</span></li>
	<li><span style="line-height: 1.5;">Establecer las políticas necesarias para implementar el Teletrabajo al interior de la empresa conforme a la Ley 884 de 2012.</span></li>
</ol>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 53°:</strong> Son obligaciones especiales del trabajador:</span></p>
<ol style="list-style-type: upper-alpha;">
	<li><b>Sobre el Respeto, la Lealtad y la Buena Conducta</b>
<ol>
	<li>Guardar el debido respeto y subordinación a sus superiores</li>
	<li><span style="line-height: 1.5;">Guardar el debido respeto a sus compañeros de trabajo.</span></li>
	<li><span style="line-height: 1.5;">Guardar buena conducta en todo sentido y obrar con espíritu de leal </span>colaboración en el orden, moral y disciplina general de la empresa</li>
	<li><span style="line-height: 1.5;">Guardar rigurosamente la moral, la lealtad y la fidelidad en las relaciones </span>con sus superiores y compañeros.</li>
	<li><span style="line-height: 1.5;">Procurar completa armonía con sus superiores y compañeros de trabajo en </span>las relaciones personales y en la ejecución de labores.</li>
	<li><span style="line-height: 1.5;">En el trabajo, utilizar un lenguaje y actitudes apropiadas para mantener un </span>buen ambiente laboral.</li>
	<li><span style="line-height: 1.5;">Evitar los comentarios de pasillo y en especial aquellos que hagan referencia en términos negativos o descalificadores a otras personas o a la Empresa o usuarias.</span></li>
	<li><span style="line-height: 1.5;">Promover un buen ambiente de trabajo, mediante el respeto, el buen trato y </span>la comunicación sincera, respetuosa y a quien corresponde.</li>
	<li><span style="line-height: 1.5;">Mantener buenas relaciones y respetar a los clientes proveedores o </span>contratistas de la Empresa y a sus trabajadores.</li>
	<li><span style="line-height: 1.5;">Atender en forma oportuna, respetuosa, cortés y eficiente a los clientes y/o </span>usuarios de la Empresa.</li>
	<li><span style="line-height: 1.5;">Comunicar oportunamente a la empresa las observaciones que estime </span>conducentes a evitarle daños y perjuicios.</li>
	<li><span style="line-height: 1.5;">Prestar la colaboración posible en caso de siniestro o riesgo inminente que </span>afecten o amenacen las personas o las cosas de la empresa.</li>
	<li><span style="line-height: 1.5;">Informar ante las autoridades de la Empresa, de acuerdo con el orden jerárquico establecido, la comisión de hechos irregulares, fraudulentos o contrarios a los principios y políticas de ésta o a las normas legales, por parte de o con la participación de trabajadores de la Empresa o de terceros.</span></li>
	<li><span style="line-height: 1.5;">Ejecutar los trabajos que le confíen con honradez, buena voluntad y de la </span>mejor manera posible.</li>
	<li><span style="line-height: 1.5;">Hacer las observaciones, reclamos y solicitudes a que haya lugar por conducto del respectivo superior y de manera fundada, comedida y respetuosa.</span></li>
	<li>Obrar siempre, en el desarrollo de su labor, de acuerdo con las políticas, normas, instrucciones, procesos y procedimientos de la Empresa, y con las disposiciones legales.</li>
	<li><span style="line-height: 1.5;">Utilizar de manera racional los beneficios y servicios que le sean otorgados </span>por la Empresa.</li>
</ol>
</li>
	<li><b>Sobre el seguimiento de las normas y Procedimientos de la Empresa, de las </b><b>instrucciones recibidas y el cumplimiento de las Funciones Asignadas.</b>
<ol start="18">
	<li><span style="line-height: 1.5;">Observar los preceptos del presente Reglamento</span></li>
	<li><span style="line-height: 1.5;">Acatar y cumplir las órdenes e instrucciones que de manera particular le imparta la empresa o sus Representantes según el orden jerárquico establecido.</span></li>
	<li><span style="line-height: 1.5;">Recibir y aceptar las órdenes, instrucciones y correcciones relacionadas con el trabajo, con su verdadera intención que es en todo caso la de encaminar y perfeccionar los esfuerzos en provecho propio y de la empresa en general.</span></li>
	<li><span style="line-height: 1.5;">Desempeñar el cargo de acuerdo con las funciones señaladas en la descripción del oficio, así como las que sean conexas o complementarias de la labor principal, todo de acuerdo con los parámetros de calidad y eficiencia establecidos en la Empresa.</span></li>
	<li><span style="line-height: 1.5;">Cumplir las funciones dentro de las facultades delegadas o inherentes al </span>cargo desempeñado.</li>
	<li><span style="line-height: 1.5;">Preguntar, consultar o asesorarse cuando tenga dudas sobre la forma de </span>realizar el trabajo asignado.</li>
	<li><span style="line-height: 1.5;">Mantenerse permanentemente actualizado sobre toda la información, normas legales, reglamentación y demás, que guarden relación con su cargo, ya sea suministrada por la Empresa o a través de diferentes medios informativos.</span></li>
	<li><span style="line-height: 1.5;">Atender a las indicaciones que la Empresa haga por medio de carteles, o circulares, anuncios e instrucciones, procedimientos, etc., relacionados con su cargo y el servicio del mismo y de la empresa en general.</span></li>
	<li><span style="line-height: 1.5;">Responder ante la Empresa por cualquier perjuicio que ésta pueda recibir como consecuencia de su conducta negligente o imprudente o por el incumplimiento de sus deberes y obligaciones laborales.</span></li>
	<li>Cumplir estrictamente con las obligaciones de orden económico adquiridas con la Empresa.</li>
	<li><span style="line-height: 1.5;">Someterse a los controles o requisas que se requieran para obtener la mayor seguridad en el desarrollo de las labores propias de la Empresa, siempre que aquellas no lesionen o pongan en peligro la dignidad del trabajador.</span></li>
</ol>
</li>
	<li><b>Sobre la Permanencia en el Puesto de Trabajo y el cumplimiento de la </b><b>Jornada de Trabajo</b>
<ol start="29">
	<li><span style="line-height: 1.5;">Realizar personalmente la labor para la cual fue contratado en los términos </span>estipulados.</li>
	<li><span style="line-height: 1.5;">Permanecer durante la jornada de trabajo en el sitio o lugar en donde debe desempeñar sus labores, realizando sus tareas con dedicación, siendo prohibido, salvo orden superior, pasar al puesto de trabajo de otros compañeros a fin de evitar perturbar las labores de los mismos.</span></li>
	<li><span style="line-height: 1.5;">Asistir puntualmente al sitio de trabajo, según el horario establecido, así </span>como a las actividades laborales convocadas por la Empresa.</li>
	<li><span style="line-height: 1.5;">Observar estrictamente las disposiciones de la Empresa para la solicitud de permisos y comprobación de incapacidades por enfermedad, calamidad doméstica y similar conforme al presente Reglamento.</span></li>
	<li><span style="line-height: 1.5;">Laborar tiempo suplementario cuando así lo requiera la Empresa.</span></li>
	<li><span style="line-height: 1.5;">Cumplir a cabalidad con los tiempos establecidos de descanso durante la </span>jornada laboral.</li>
</ol>
</li>
	<li><b>Sobre el Manejo de la Información de la Empresa y la actualización de </b><b>Datos Personales</b>
<ol start="35">
	<li><span style="line-height: 1.5;">Brindar información verídica en todos los casos.</span></li>
	<li><span style="line-height: 1.5;">Guardar absoluta reserva en relación con los manuales de procedimientos, programas de sistematización, información atinente a asuntos internos o administrativos de la Empresa, de cualquier índole, o información relacionada con los clientes o usuarios de la Empresa</span></li>
	<li><span style="line-height: 1.5;">No comunicar a terceros, salvo autorización expresa, las informaciones que sean de naturaleza reservada y cuya divulgación pueda ocasionar perjuicios a la empresa, lo que no obsta para denunciar delitos comunes o violaciones del contrato o de las normas legales de trabajo ante las autoridades competentes.</span></li>
	<li>Registrar en las oficinas de la empresa su domicilio y dirección y dar aviso oportuno de cualquier cambio que ocurra (Artículo 58 C.S.T.)</li>
	<li><span style="line-height: 1.5;">Registrar a la administración los cambios de su Estado Civil, nombre del cónyuge o compañera permanente, hijos, edades, dirección, etc. y toda aquella información personal que la Empresa requiera mantener actualizada, cada vez que ocurra algún cambio, es necesario mencionar que esta información será confidencia y solo será utilizada para trámites ante el sistema de seguridad social integral, respetando en todo caso la normativa del habas data.</span></li>
	<li><span style="line-height: 1.5;">Utilizar únicamente el software adquirido legalmente por la Empresa. </span></li>
	<li><span style="line-height: 1.5;">No dar o autorizar la clave personal entregada por la Empresa, a otros compañeros de trabajo o terceros, durante la duración del contrato de trabajo y aún luego de la terminación del mismo</span></li>
</ol>
</li>
	<li><b>Sobre el Manejo de Materiales, Equipos y Maquinaria</b>
<ol start="42">
	<li>Utilizar en forma racional y sensata los elementos, herramientas o equipos de trabajo y demás recursos que la Empresa ponga a su disposición para el cumplimiento de las labores.</li>
	<li><span style="line-height: 1.5;">Conservar y restituir en buen estado salvo el deterioro natural, los instrumentos y útiles que le haya proporcionado la Empresa para la ejecución de sus labores.</span></li>
	<li><span style="line-height: 1.5;">Devolver a la empresa los materiales e insumos sobrantes.</span></li>
	<li><span style="line-height: 1.5;">Evitar e impedir la pérdida, daño o desperdicio de materia prima, equipos, </span>muebles, energía u otros elementos de la Empresa.</li>
	<li><span style="line-height: 1.5;">Observar rigurosamente las instrucciones y precauciones de uso y/o las medidas que le indique su respectivo Jefe para el manejo de las máquinas o instrumentos de trabajo.</span></li>
	<li><span style="line-height: 1.5;">Evitar que terceras personas utilicen sus materiales de trabajo, enseres, mobiliario, equipos y elementos de oficina, y en general los muebles e inmuebles de propiedad o que estén al servicio de la Empresa; o que se lucren de servicios o beneficios que ésta haya dispuesto para sus trabajadores.</span></li>
	<li><span style="line-height: 1.5;">Mantener en adecuado estado de orden y limpieza su puesto de trabajo, así </span>como los equipos y recursos entregados para tal fin.</li>
</ol>
</li>
	<li><b>Sobre la Presentación Personal, Uso de la Dotación y Carnet de </b><b>Identificación de la Empresa</b>
<ol start="49">
	<li><span style="line-height: 1.5;">Asistir al trabajo en adecuadas condiciones de presentación personal.</span></li>
	<li><span style="line-height: 1.5;">Utilizar la dotación suministrada en aquellos casos en que la empresa lo y en aquellos casos que la empresa los requiera, utilizar la dotación suministrada.</span></li>
	<li><span style="line-height: 1.5;">Portar en un sitio visible, el carné establecido por la Empresa para la </span>identificación personal de sus trabajadores, en el lugar y horas de trabajo.</li>
</ol>
</li>
	<li><b>Sobre Seguridad y salud en el trabajo y Seguridad Industrial</b>
<ol start="52">
	<li><span style="line-height: 1.5;">Observar las medidas preventivas higiénicas prescritas por el médico, la persona encargada de la empresa o por las autoridades del ramo y observar con suma diligencia y cuidado las instrucciones y órdenes preventivas de accidentes o de enfermedades profesionales.</span></li>
	<li><span style="line-height: 1.5;">Cumplir con las normas de Seguridad Industrial y Seguridad y salud en el trabajo, en especial las que tienen que ver con el uso de los Elementos de Protección Personal.</span></li>
	<li><span style="line-height: 1.5;">Participar en los programas preventivos y de capacitación que programe el </span>área de Salud Ocupacional o el área de formación.</li>
	<li><span style="line-height: 1.5;">Abstenerse de realizar labores que afecten su salud u ocasionen desgaste a su organismo en forma tal que le impidan prestar adecuadamente el servicio contratado.</span></li>
	<li><span style="line-height: 1.5;">Informar en un lapso de 24 horas una vez ocurrido, todo accidente o incidente de trabajo por leve que sea, al Jefe Inmediato, Supervisor o Coordinación de Salud Ocupacional.</span></li>
	<li><span style="line-height: 1.5;">Cumplir las citas, tratamientos, terapias y períodos de inactividad por incapacidad que ordene la institución respectiva del Sistema de Seguridad Social a la cual se encuentre afiliado.</span></li>
	<li><span style="line-height: 1.5;">Cumplir las instrucciones, reglamentos y determinaciones de prevención de riesgos adoptados en forma general o específica o que se encuentren dentro de los programas de Gestión en Seguridad y Salud en el Trabajo SG-SST.</span></li>
	<li><span style="line-height: 1.5;">Cumplir a cabalidad la Política de Prevención de Consumo de Sustancias Psicoactivas con que cuenta la empresa, y demás instrucciones impartidas con relación a este tema.</span></li>
</ol>
</li>
	<li><b>Sobre las Citaciones, asistencia a reuniones convocadas, por la empresa, así como las responsabilidades y Recomendaciones del COPASST y el Comité de Convivencia Laboral.</b>
<ol start="60">
	<li><span style="line-height: 1.5;">Asistir puntualmente a las citaciones / convocatorias que reciba por parte del Comité de Convivencia Laboral de la Empresa y prestar su colaboración suministrando de manera real y fidedigna la información solicitada de manera oficial por éste.</span></li>
	<li><span style="line-height: 1.5;">Acatar y cumplir los acuerdos y recomendaciones emitidos con ocasión a </span>las investigaciones que adelante el Comité de Convivencia.</li>
	<li><span style="line-height: 1.5;">Cumplir a cabalidad con las responsabilidades y tareas asignadas, que le correspondan como miembro del COPASST y/o Comité de Convivencia laboral. Ya sea como principales o suplentes y cargo o posición que ocupe dentro del mismo.</span></li>
	<li><span style="line-height: 1.5;">Concurrir cumplidamente a las reuniones generales o de grupos, ordinarias o extraordinarias, convocadas por la Empresa, el Copasst o el Comité de Convivencia Laboral, y todas aquellas que se requieran con ocasión a su trabajo.</span></li>
</ol>
</li>
</ol>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>PROHIBICIONES ESPECIALES PARA LA EMPRESA Y LOS TRABAJADORES</b></h4>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 54°:</strong> Se prohibe al empleador:</span></p>
<ol>
	<li><span style="font-weight: 400;"> Deducir, retener o compensar suma alguna del monto de los salarios y prestaciones en dinero que corresponda a los trabajadores sin autorización previa de estos para cada caso o sin mandamiento judicial, con excepción de los siguientes:</span>
<ol type="a">
	<li><span style="font-weight: 400;">Respecto de salarios pueden hacerse deducciones, retenciones o compensaciones en los casos autorizados por los artículos 113, 150, 151, 152, y 400 del Código Sustantivo de Trabajo, en concordancia con lo establecido en la Ley 1429 de 2010 y Ley 1527 de 2012 (Normatividad de libranzas), conforme a la capacidad de endeudamiento del solicitante.</span></li>
	<li>Las cooperativas pueden ordenar retenciones hasta de 50% cincuenta por ciento de salarios y prestaciones, para cubrir sus créditos, en forma y en los casos en que la ley los autorice.</li>
	<li><span style="font-weight: 400;">En cuanto a la cesantía y las pensiones de jubilación, la empresa puede retener el valor respectivo en el caso del artículo 250 del Código Sustantivo de Trabajo.</span></li>
</ol>
</li>
	<li><span style="line-height: 1.5;">Obligar en cualquier forma a los trabajadores a comprar mercancías o víveres en almacenes que establezca la empresa.</span></li>
	<li><span style="font-weight: 400;">Exigir o aceptar dinero del trabajador como gratificación para que se admita en el trabajo o por otro motivo cualquiera que se refiera a las condiciones de éste.</span></li>
	<li><span style="font-weight: 400;"> Limitar o presionar en cualquier forma a los trabajadores en el ejercicio de su derecho de asociación.</span></li>
	<li><span style="font-weight: 400;"> Imponer a los trabajadores obligaciones de carácter religioso o político o dificultarles o impedirles el ejercicio del derecho al sufragio.</span></li>
	<li><span style="font-weight: 400;"> Hacer, o autorizar propaganda política en los sitios de trabajo.</span></li>
	<li><span style="font-weight: 400;"> Hacer o permitir todo género de rifas, colectas o suscripciones en los mismos sitios.</span></li>
	<li><span style="font-weight: 400;"> Emplear en las certificaciones de que trata el ordinal 7o del artículo 57 del Código Sustantivo de Trabajo signos convencionales que tienden a perjudicar a los interesados o adoptar el sistema de "lista negra", cualquiera que sea la modalidad que se utilice para que no se ocupe en otras empresas a los trabajadores que se separen o sean separados del servicio.</span></li>
	<li><span style="font-weight: 400;"> Cerrar intempestivamente la empresa. Si lo hiciera además de incurrir en sanciones legales deberá pagar a los trabajadores los salarios, prestaciones, e indemnizaciones por el lapso que dure cerrada la empresa. Así mismo cuando se compruebe que el empleador en forma ilegal a retenido o disminuido colectivamente los salarios a los trabajadores, la cesación de actividades de éstos, será imputable a aquel y les dará derecho a reclamar los salarios correspondientes al tiempo de suspensión de labores.</span></li>
	<li><span style="font-weight: 400;"> Despedir sin justa causa comprobada a los trabajadores que les hubieren presentado pliego de peticiones desde la fecha de presentación del pliego y durante los términos legales de las etapas establecidas para el arreglo del conflicto.</span></li>
	<li><span style="font-weight: 400;"> Ejecutar o autorizar cualquier acto que vulnere o restrinja los derechos de los trabajadores o que ofenda su dignidad (artículo 59, C.S.T.).</span></li>
</ol>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 55°:</strong> Se prohibe a los trabajadores:</span></p>
<ol style="list-style-type: upper-alpha;">
	<li><b>Sobre el respeto, la Lealtad y la Buena Conducta</b>
<ol>
	<li> Tratar indebidamente o en forma descuidada o irrespetuosa a los Compañeros, Jefes, clientes o usuarios de la Empresa.</li>
	<li><span style="line-height: 1.5;">Coartar la libertad para trabajar o no trabajar o para afiliarse o no a un sindicato o </span>permanecer en él o retirarse.</li>
	<li><span style="line-height: 1.5;">Presentar conductas que puedan ir en contra de los intereses de la empresa, o </span>usuaria, relacionadas con el objeto social y giro ordinario de sus negocios.</li>
</ol>
</li>
	<li><b>Sobre el cumplimiento de las Funciones Asignadas</b>
<ol start="4">
	<li><span style="line-height: 1.5;">Disminuir intencionalmente el ritmo de ejecución del trabajo, suspender labores, promover suspensiones intempestivas del trabajo e incitar a su declaración o mantenimiento, sea que se participe o no en ellas.</span></li>
	<li><span style="line-height: 1.5;">Dejar de elaborar los informes de trabajo propios de su cargo o que se le soliciten.</span></li>
	<li><span style="line-height: 1.5;">Suspender la ejecución de un trabajo sin justa causa o negarse a realizarlo.</span></li>
	<li><span style="line-height: 1.5;">Cambiar métodos de trabajo sin autorización de sus superiores.</span></li>
	<li><span style="line-height: 1.5;">Negarse sin causa justa a cumplir una orden del superior, siempre que ella no </span>lesione su dignidad.</li>
	<li><span style="line-height: 1.5;">Atender durante la jornada de trabajo asuntos u ocupaciones distintas a las que la </span>Empresa le encomiende.</li>
</ol>
</li>
	<li><b>Sobre la permanencia en el sitio de trabajo, Ausencias y Visitas:</b>
<ol start="10">
	<li><span style="font-weight: 400;">Faltar al trabajo sin justa causa o sin permiso de la empresa, excepto en los casos </span>de huelga, en los cuales debe abandonar el lugar de trabajo. </li>
	<li><span style="line-height: 1.5;">Salir de las dependencias de la Empresa, en horas hábiles de trabajo, sin previa </span>autorización.</li>
	<li><span style="line-height: 1.5;">Hacer cambios en los horarios de trabajo sin previa autorización del jefe.</span></li>
	<li><span style="line-height: 1.5;">No regresar al trabajo una vez termine la acción del permiso otorgado para consulta médica en la entidad de Seguridad Social donde se encuentre afiliado, o cualquier otra diligencia.</span></li>
	<li><span style="line-height: 1.5;">Atender visitas personales en horas de trabajo.</span></li>
	<li>Permanecer en zonas o lugares distintos de aquellos en que realice su trabajo habitual, sin causa que lo justifique o sin autorización de la empresa, la usuario o sus representantes.</li>
	<li><span style="line-height: 1.5;">Encontrarse en el local de trabajo, sin autorización fuera de la jornada laboral.</span></li>
</ol>
</li>
	<li><b>Sobre el uso de Materiales, Herramientas y Equipos de Trabajo:</b>
<ol start="17">
	<li><span style="line-height: 1.5;">Sustraer del almacén, taller o establecimiento: útiles de trabajo, materias primas, </span>herramientas o productos, sin permiso de la empresa.</li>
	<li><span style="line-height: 1.5;">Usar los útiles, herramientas, equipos electrónicos, software o hardware, suministradas por la empresa en objetivos distintos al trabajo contratado (Artículo 60 C.S.T.)</span></li>
	<li><span style="line-height: 1.5;">Conservar armas de cualquier clase en el sitio de trabajo a excepción de las que </span>con autorización legal puedan llevar los Guardas de Seguridad.</li>
</ol>
</li>
	<li><b>Sobre el manejo de Documentación, Software y Reserva de la información</b>
<ol start="20">
	<li><span style="line-height: 1.5;">Llevar fuera de las dependencias u oficinas de la Empresa, sin autorización previa, manuales, programas (software) y documentos de cualquier naturaleza de propiedad de la Empresa, o prestarlos o fotocopiarlos sin autorización.</span></li>
	<li><span style="line-height: 1.5;">Retirar documentos de los archivos, oficinas o lugar de trabajo, o revelar su </span>contendido sin autorización expresa para ello.</li>
	<li><span style="line-height: 1.5;">Suministrar a terceros sin autorización expresa, papelería, especificaciones y datos </span>relacionados con la organización, los sistemas y procedimientos de la Empresa.</li>
	<li><span style="line-height: 1.5;">Usar software sin la previa y expresa autorización de la Empresa.</span></li>
</ol>
</li>
	<li><b>Sobre la divulgación de Información</b>
<ol start="24">
	<li><span style="line-height: 1.5;">Escribir o fijar carteles, pancartas, o avisos de cualquier clase, en las </span>instalaciones de la Empresa, sin previa autorización de la misma.</li>
	<li><span style="line-height: 1.5;">Hacer encuestas o distribuir impresos sin autorización de la Empresa.</span></li>
	<li><span style="line-height: 1.5;">Insertar publicaciones en las carteleras que conlleven agravios personales a sus </span>Directivos y trabajadores o información política de cualquier género.</li>
	<li><span style="line-height: 1.5;">Hacer colectas, rifas o suscripciones o cualquier otra clase de propaganda en los </span>lugares de trabajo.</li>
	<li>Realizar cualquier acción u omisión que pueda dar origen a reclamaciones justificadas contra la Empresa.</li>
</ol>
</li>
	<li><b>Sobre la seguridad en el Trabajo</b>
<ol start="29">
	<li><span style="line-height: 1.5;">Ejecutar cualquier acto que ponga en peligro su seguridad, la de sus compañeros de trabajo, la de sus superiores o la de terceras personas, o que amenace o perjudique los equipos, elementos, edificios, oficinas o salas de trabajo.</span></li>
</ol>
</li>
	<li><b>Sobre la seguridad y salud en el trabajo y Seguridad Industrial</b>
<ol start="30">
	<li><span style="line-height: 1.5;">Portar en el sitio de trabajo aunque no los ingiera: licor, narcóticos o Sustancias </span>psicoactivas.</li>
	<li><span style="line-height: 1.5;">Realizar las tareas asignadas sin el cumplimiento de las normas y procedimientos </span>de Seguridad Industrial y Seguridad y salud en el trabajo.</li>
	<li><span style="line-height: 1.5;">Presentarse al trabajo en estado de embriaguez o bajo la influencia del licor o de </span>sustancias psicoactivas, o ingerirlos durante la jornada laboral.</li>
</ol>
</li>
</ol>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>CAPÍTULO XII</b></h4>
<h4 style="text-align: center;"><b>ESCALA DE FALTAS Y SANCIONES DISCIPLINARIAS</b></h4>
<p><span style="font-weight: 400;"><strong>ARTICULO 56°</strong> La empresa no puede imponer a sus trabajadores sanciones no previstas en este reglamento, en pactos, convenciones colectivas, fallos arbitrales o en contrato de trabajo (artículo 114, C.S.T.).</span></p>
<p><span style="font-weight: 400;">Los trabajadores pueden ser sancionados por el empleador en virtud de incumplimientos contractuales, de acuerdo con la graduación de faltas y sanciones establecidas en las disposiciones legales y en este reglamento</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 1</strong>: También se tendrán en cuenta para efectos de este artículo, las escalas de faltas y sanciones especiales o particulares de las empresas usuarias.</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 57°</strong> El incumplimiento de las obligaciones y deberes propios del personal al servicio de Servilabor sas será constitutivo de falta y dará lugar a las correspondientes sanciones disciplinarias, se establecen las siguientes clases de faltas leves y sus sanciones disciplinarias así:</span></p>
<p><b>FALTAS Y SANCIONES:</b></p>
<p><span style="font-weight: 400;">Se establecen las siguientes clases de faltas leves y sus sanciones disciplinarías, así:</span></p>
<ol style="list-style-type: upper-alpha;">
	<li><span style="font-weight: 400;"></span><em><b>Ausencias y Retardos:</b></em>
<ol>
	<li><span style="font-weight: 400;"> El retardo hasta de quince (15) minutos en la hora de entrada sin excusa suficiente en el lapso de tres meses, cuando no cause perjuicio de consideración a la empresa, ó</span></li>
	<li>El incumplimiento del horario del almuerzo y/o los descansos durante la jornada <span style="font-weight: 400;"><span style="font-weight: 400;">laboral en el lapso de tres meses, implica:<br />
 </span></span>
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez    </span></td>
<td>Un llamado de atención verbal</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Llamado de atención escrito</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>Suspensión del trabajo hasta por tres días</td>
</tr>
<tr>
<td>Por cuarta vez</td>
<td>
<p>Se constituirá en falta grave que dará lugar a la terminación del contrato de trabajo con justa causa</p>
</td>
</tr>
</tbody>
</table class="table table-bordered">
</li>
	<li>
<p><span style="font-weight: 400;">La falta injustificada a laborar durante la mañana, la tarde o el turno correspondiente, sin excusa suficiente cuando no cause perjuicio de consideración a la empresa, implica:</span></p>
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Suspensión hasta por tres (3) días.</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por ocho (8) días.</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
</li>
	<li>
<p>La falta injustificada a laborar durante la jornada laboral cuando no cause perjuicio de consideración a la empresa, implica:</p>
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Suspensión hasta por ocho (8) días</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por dos (2) meses</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
</li>
	<li>
<p><span style="font-weight: 400;">Salir de las dependencias de la compañía durante las horas de trabajo sin previa </span><span style="font-weight: 400;">autorización </span></p>
</li>
	<li>
<p><span style="font-weight: 400;">Ausentarse de su puesto de trabajo injustificadamente, siempre y cuando no cause </span><span style="font-weight: 400;">perjuicios de consideración a la empresa.</span></p>
</li>
	<li>
<p><span style="font-weight: 400;">Prolongar las ausencias breves y justificadas por tiempo superior al necesario, </span><span style="font-weight: 400;">implica:<br />
 </span></p>
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Llamado de atención por escrito.</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por tres (3) días.</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
<p><span style="font-weight: 400;"> </span></p>
</li>
</ol>
</li>
	<li>
<p><em><b>Incumplimiento de las funciones asignadas</b></em></p>
<ol start="8">
	<li><span style="font-weight: 400;"> Cambiar los turnos de labor o días de descanso asignados sin autorización, ó</span>
<ol style="list-style-type: lower-alpha;">
	<li><span style="font-weight: 400;"> Disminuir el ritmo de trabajo intencionalmente o no cumplir con las metas </span>previamente asignadas sin justificación, ó</li>
</ol>
</li>
	<li>No atender los requerimientos de la empresa para laborar en horario adicional, siempre que la misma cuente con autorización para ello, sin justificación, implica:<br />
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Llamado de atención por escrito.</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por tres (3) días.</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
</li>
	<li>
<p>Incumplir sin justa causa las órdenes de su superior, siempre que estas no lesionen la dignidad del trabajador, implica:</p>
<table class="table table-bordered">
<tbody>
<tr>
<td>Por primera vez </td>
<td>Suspensión hasta por tres (3) días.</td>
</tr>
<tr>
<td>Por segunda vez </td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
</li>
	<li>
<p><span style="font-weight: 400;">Hacer trabajos distintos a su oficio dentro de la empresa, sin la debida autorización, </span><span style="font-weight: 400;">ó 1</span></p>
</li>
	<li>
<p><span style="font-weight: 400;">Incurrir en errores debido a descuidos, aunque no ocasionen daños o afecten la seguridad material, de los equipos y herramientas, pero que si generen gastos o perjuicios para la empresa, ó</span></p>
</li>
	<li>
<p><span style="font-weight: 400;">Incumplir el Manual de Funciones del Trabajador, ó</span></p>
</li>
	<li>
<p><span style="font-weight: 400;">No trabajar de acuerdo con los métodos y/o sistemas implantados por la empresa, ó</span></p>
</li>
	<li>
<p><span style="font-weight: 400;">Contribuir a hacer peligroso el lugar de trabajo, ingresando a las dependencias de la empresa cualquier tipo de elementos que pongan en riesgo la seguridad de las </span><span style="font-weight: 400;">personas o instalaciones de la misma, ó</span></p>
</li>
	<li>
<p><span style="font-weight: 400;">Distraer a sus compañeros en el cumplimiento de sus funciones en la jornada </span><span style="font-weight: 400;">laboral</span></p>
</li>
	<li>
<p><span style="font-weight: 400;">El retraso, negligencia o descuido en el cumplimiento de sus funciones.</span></p>
</li>
	<li>
<p><span style="font-weight: 400;">La displicencia (así sea leve), en el trato para con sus compañeros, superiores, </span><span style="font-weight: 400;">subordinados, clientes y usuarios de la empresa, implica:<br />
 </span></p>
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Llamado de atención por escrito.</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por cinco (5) días.</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
</li>
	<li>
<p>No cumplir con sus responsabilidades o tareas asignadas en calidad de miembro del COPASST o del Comité de Convivencia Laboral ya sea como miembro principal o suplente, implica:</p>
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez    </span></td>
<td>Un llamado de atención verbal</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Llamado de atención por escrito</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>Suspensión hasta por ocho (8) días</td>
</tr>
<tr>
<td>Por cuarta vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
</li>
</ol>
<p>&nbsp;</p>
</li>
	<li><strong><em>Publicación y uso de Información</em></strong>
<ol start="20">
	<li>Fijar, remover o dañar información o material de los carteles, o colocar avisos de cualquier clase en las dependencias de la empresa sin previa autorización de las gerencias, o destruir o dañar de cualquier forma las existentes. Salvo información referente al derecho de asociación:<br />
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Suspensión hasta por tres (3) días.</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por cinco (5) días.</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
</li>
	<li>
<p>Ingresar dispositivos de almacenamiento para compartir o grabar información de la empresa sin previa autorización:</p>
<table class="table table-bordered">
<tbody>
<tr>
<td>Por primera vez </td>
<td>Suspensión hasta por siete (7) días</td>
</tr>
<tr>
<td>Por segunda vez </td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
</li>
	<li>
<p>Instalar software en los computadores de la empresa, sin la autorización escrita de las gerencias de la misma, así este cuente con la licencia otorgada por el fabricante en los términos de ley:</p>
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Llamado de atención por escrito.</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por ocho (8) días.</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
</li>
	<li>
<p>Utilizar la red de Internet dispuesta para la empresa, tanto en lo relativo a la red, como al envío o recepción de mensajes para fines distintos a los propios del desempeño de su cargo:</p>
<table class="table table-bordered">
<tbody>
<tr>
<td>Por primera vez </td>
<td>Suspensión hasta por ocho (8) días</td>
</tr>
<tr>
<td>Por segunda vez </td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
</li>
</ol>
</li>
	<li><em><strong>Suministro de información oportuna y confiable:</strong></em>
<ol start="24">
	<li>Hacer mal uso o engañar intencionalmente a la empresa para obtener préstamos de cualquier índole o para acceder a liquidaciones parciales de cesantías:<br />
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Suspensión hasta por tres (3) días.</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por siete (7) días.</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
</li>
	<li>
<p>Ocultar faltas cometidas contra la empresa por parte de algún trabajador de la misma:</p>
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Llamado de atención por escrito.</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por cinco (5) días.</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
</li>
</ol>
</li>
	<li><em><strong>Uso adecuado de materiales y/o equipos</strong></em>
<ol start="26">
	<li>
<p>Dañar por un uso inadecuado los objetos de la empresa o de sus compañeros de trabajo:</p>
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Suspensión hasta por tres (3) días.</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por ocho (8) días.</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
</li>
	<li>
<p><span style="font-weight: 400;">Dar mal uso, o desperdiciar las materias primas o insumos, que coloque la </span><span style="font-weight: 400;">empresa a disposición del trabajador para ejecutar su labor o</span></p>
</li>
	<li>
<p><span style="font-weight: 400;">Usar medios electrónicos e informáticos, mecánicos de la empresa para </span><span style="font-weight: 400;">asuntos particulares, sin la debida autorización:<br />
 </span></p>
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Llamado de atención por escrito.</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por tres (3) días.</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
<p><span style="font-weight: 400;"> </span></p>
</li>
</ol>
</li>
	<li><em><strong>Uso del uniforme / dotación</strong></em>
<ol start="29">
	<li>
<p><span style="font-weight: 400;">No usar el uniforme asignado por la empresa, y/o usarlo sucio o en mal estado, y/o no portar los distintivos o escarapelas que el empleador entregue al empleado, en la forma indicada, ó</span></p>
</li>
	<li>
<p><span style="font-weight: 400;">Rehusarse a mostrar o entregar el carnet de la empresa cuando así se lo exija </span><span style="font-weight: 400;">el empleador o los clientes de este, implica:<br />
 </span></p>
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Llamado de atención por escrito.</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por tres (3) días.</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
</li>
</ol>
</li>
	<li><b><i>Asistencia a Reuniones y Capacitaciones</i></b>
<ol start="31">
	<li>
<p><span style="font-weight: 400;">No asistir a cursos de capacitación, entrenamiento, perfeccionamiento o reunión ordinaria o extraordinaria que haya organizado o indicado la Empresa, el Copasst o el Comité de Convivencia laboral o hacerlo impuntualmente bien sea que se realice dentro o fuera de las dependencias de la misma, ó</span></p>
</li>
	<li>
<p><span style="font-weight: 400;">No concurrir cumplidamente a las reuniones generales o de grupo, organizada </span><span style="font-weight: 400;">y convocada debidamente por la empresa, implica:<br />
 </span></p>
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Llamado de atención por escrito.</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por cinco (5) días.</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
<p><span style="font-weight: 400;"> </span></p>
</li>
</ol>
</li>
	<li><em><strong>Cumplimiento normas de Higiene y Seguridad Industrial</strong></em>
<ol start="33">
	<li>
<p><span style="font-weight: 400;">Incumplir las normas de higiene y seguridad industrial, implica:<br />
 </span></p>
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Suspensión hasta por cinco (5) días.</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por veinte (20) días.</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
<p><span style="font-weight: 400;"> </span></p>
</li>
	<li>
<p><span style="font-weight: 400;">No asistir a las capacitaciones y actividades de promoción y prevención de la </span><span style="font-weight: 400;">salud programadas por la Empresa, ó</span></p>
</li>
	<li>
<p><span style="font-weight: 400;">No reportar en debida forma los incidentes y accidentes de trabajo sufridos </span><span style="font-weight: 400;">dentro de las 24 horas siguientes al accidente, implica:<br />
 </span></p>
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Llamado de atención por escrito.</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por cinco (5) días.</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
<p><span style="font-weight: 400;"> </span></p>
</li>
</ol>
</li>
	<li>
<p><b><i>Presentarse a trabajar bajo el efecto del Alcohol o Sustancias </i></b><b><i>Psicoactivas</i></b></p>
<ol start="36">
	<li>
<p>Fumar en recintos cerrados de las dependencias de la empresa, y/o durante la jornada laboral:</p>
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Llamado de atención por escrito.</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por cinco (5) días.</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
</li>
	<li>
<p>Consumir o portar en el sitio de trabajo alcohol, narcóticos, drogas enervantes o sustancias psicoactivas:</p>
<table class="table table-bordered">
<tbody>
<tr>
<td>Por primera vez </td>
<td>Suspensión hasta por cinco (5) días</td>
</tr>
<tr>
<td>Por segunda vez </td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
</li>
	<li>
<p>Presentarse a trabajar bajo la influencia del alcohol o narcóticos o drogas enervantes, implicará:</p>
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Suspensión hasta por tres (3) días.</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por ocho (8) días.</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
</li>
</ol>
</li>
	<li>
<p><b><i>Cumplir con los Acuerdos y Recomendaciones del Comité de </i></b><b><i>Convivencia Laboral:</i></b></p>
<ol start="39">
	<li>
<p><span style="font-weight: 400;">No cumplir con los acuerdos y recomendaciones emitidos por el Comité de Convivencia laboral con motivo de las investigaciones que adelante y/o las actividades de prevención y corrección de las conductas que puedan constituir acoso laboral, implicará:<br />
 </span></p>
<table class="table table-bordered">
<tbody>
<tr>
<td><span style="font-weight: 400;">Por primera vez</span></td>
<td>Llamado de atención por escrito.</td>
</tr>
<tr>
<td>Por segunda vez</td>
<td>Suspensión hasta por cinco (5) días.</td>
</tr>
<tr>
<td>Por tercera vez</td>
<td>
<p>No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</p>
</td>
</tr>
</tbody>
</table>
<p><span style="font-weight: 400;"> </span></p>
</li>
</ol>
</li>
</ol>
<p><span style="font-weight: 400;">El incumplimiento o la infracción por parte del trabajador, de las demás obligaciones o prohibiciones legales, contractuales o reglamentarias, se sancionará con suspensión en el trabajo hasta por ocho (8) días, la primera vez, y suspensión en el trabajo hasta por dos (2) meses, en caso de reincidencia, según la gravedad de la falta, el perjuicio causado, el número de reincidencias y la responsabilidad que al trabajador se le haya asignado.</span></p>
<p><span style="font-weight: 400;">El incumplimiento del Manual de responsabilidades de cada trabajador, se sancionará con suspensión en el trabajo hasta por ocho (8) días, la primera vez, y suspensión en el trabajo hasta por dos (2) meses, en caso de reincidencia; de continuar manifestándose la</span></p>
<p><span style="font-weight: 400;"></span><span style="font-weight: 400;">misma causal No se aplicará sanción disciplinaria, se considerará justa causa de terminación del contrato, de conformidad con el presente reglamento.</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 1:</strong> Falta Leve: Incumplimiento que no es perjudicial para el desarrollo normal de la actividad productiva de la empresa, ni causa daño considerable a instalaciones o riesgo de accidente a él mismo o a sus compañeros</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 2:</strong> En relación con las faltas y sanciones disciplinarias de que trata este artículo, se deja claramente establecido que la Empresa no reconocerá ni pagará el salario correspondiente al tiempo dejado de trabajar por causa de cualquiera de tales faltas y su correspondiente sanción.</span></p>
<p><span style="font-weight: 400;">La imposición de multas no impide que la empresa prescinda del pago del salario correspondiente al tiempo dejado de trabajar. El valor de las multas se consignará en cuenta especial para dedicarse exclusivamente a premios o regalos para los trabajadores del establecimiento que más puntual y eficientemente, cumplan sus obligaciones.</span></p>
<p><span style="font-weight: 400;">ARTICULO 51° Se califican como FALTAS GRAVES y, por lo tanto, dan lugar a la terminación del contrato de trabajo por decisión unilateral de la Empresa, por justa causa, además de las establecidas en el artículo 62 del Código Sustantivo del Trabajo o en el contrato de trabajo, las siguientes faltas:</span></p>
<ol style="list-style-type: upper-alpha;">
	<li><b><i> Sobre el Respeto la lealtad y la buena conducta</i></b>
<ol>
	<li>
<p>El incumplimiento del deber de fidelidad a la empresa o a la usuaria.</p>
</li>
	<li>
<p>Toda actuación que suponga discriminación por razón de raza, sexo, religión lengua, opinión, lugar de nacimiento o vecindad, o cualquier otra condición personal o social.</p>
</li>
	<li>
<p>La adopción de acuerdos manifiestamente ilegales, que causen grave perjuicio para la empresa usuaria o el empleador.</p>
</li>
	<li>
<p>La obstaculización del ejercicio de las libertades públicas y de los derechos sindicales.</p>
</li>
	<li>
<p>La realización de actos dirigidos a coartar el libre ejercicio del derecho a la huelga.</p>
</li>
	<li>
<p>La participación en huelgas a quienes la tengan expresamente prohibida por la ley.</p>
</li>
	<li>
<p>La realización de actos encaminados a limitar la libre expresión del pensamiento, las ideas y las opiniones.</p>
</li>
	<li>
<p>Las conductas constitutivas de delito doloso relacionados con el servicio o que causen daño a la empresa usuaria o a Servilabor S.A.S. La grave desconsideración con los superiores, compañeros o subordinados. El atentado grave a la dignidad de los superiores o compañeros o funcionarios de la empresa usuaria o de Servilabor sas.</p>
</li>
	<li>
<p>La grave desconsideración con los superiores, compañeros o subordinados.</p>
</li>
	<li>
<p>El atentado grave a la dignidad de los superiores o compañeros de trabajo, o funcionarios de la empresa usuaria o de Servilabor S.A.S.</p>
</li>
	<li>
<p>El fraude, deslealtad o abuso de confianza en las gestiones encomendadas o la apropiación, hurto o robo de bienes de propiedad de la empresa, de compañeros o de cualesquiera otras personas dentro de las dependencias de la empresa.</p>
</li>
	<li>
<p>La realización de actividades que impliquen competencia desleal a la empresa.</p>
</li>
	<li>
<p>El acoso sexual</p>
</li>
	<li>
<p>La reincidencia o reiteración en la comisión de faltas graves, considerando como tal aquella situación en la que, con anterioridad a la comisión del hecho, el trabajador hubiese sido sancionado dos o más veces por faltas leves o graves, aún de distinta naturaleza, durante un periodo de un año.</p>
</li>
	<li>
<p>Engañara a la empresa con relación al uso y/o justificación de incapacidades, permisos, licencias, anticipos de cesantía, créditos, o demás beneficios que ésta otorgue a los trabajadores.</p>
</li>
	<li>
<p>Incurrir en conductas que impliquen agresión, desavenencia, injuria, falta de respeto o ultraje físico, verbal o moral con relación a los superiores jerárquicos, compañeros de trabajo, clientes o usuarios de los servicios.</p>
</li>
	<li>
<p>Permitir o facilitar, por acción u omisión, operaciones irregulares o ilícitas.</p>
</li>
	<li>
<p>Tolerar el incumplimiento de las políticas y normas de la Empresa por parte de los empleados a su cargo.</p>
</li>
	<li>
<p>Cualquier clase de comportamiento escandaloso, dentro o fuera de las instalaciones de la Empresa, y que a juicio de ésta ponga en entredicho la moralidad de la Institución y su buen nombre.</p>
</li>
	<li>
<p>Afectar indebidamente las cuentas de la Empresa o de sus usuarias.</p>
</li>
</ol>
</li>
	<li>
<p><em><strong>Sobre el seguimiento de las normas y procedimiento de la empresa, así como de las instrucciones recibidas y el cumplimiento de las funciones asignadas.</strong></em></p>
<ol>
	<li>
<p>La notoria falta de rendimiento que conlleve inhibición en el cumplimiento de las tareas encomendadas por la empresa usuaria y pueda lesionar la relación comercial de esta con Servilabor sas</p>
</li>
	<li>
<p>La falta de obediencia debida a los superiores jerárquicos establecidos por la empresa.</p>
</li>
	<li>
<p>El abuso de autoridad en el ejercicio de su cargo y funciones.</p>
</li>
	<li>
<p>La tolerancia de los superiores respecto de la comisión de faltas graves de sus subordinados.</p>
</li>
	<li>
<p>La disminución voluntaria y continuada en el rendimiento del trabajo pactado.</p>
</li>
	<li>
<p>No permitir los controles de seguridad que se apliquen en la empresa usuaria o en Servilabor sas.</p>
</li>
	<li>
<p>La grave perturbación del servicio en la empresa usuaria.</p>
</li>
	<li>
<p>La desatención y/o negligencia con los usuarios y/o clientes de la empresa o la usuaria, que cause perjuicio a la imagen de las mismas.</p>
</li>
	<li>
<p>La falta de rendimiento que afecte el normal funcionamiento de los servicios prestados a la empresa usuaria.</p>
</li>
	<li>
<p>Dedicarse en el sitio de trabajo al manejo de negocios particulares, o a realizar actividades de comercio con otros trabajadores de la Empresa o con terceros, cualquiera que sea su finalidad.</p>
</li>
	<li>
<p>Realizar operaciones o desempeñar funciones cuya ejecución esté atribuida expresa e inequívocamente a otro trabajador de la Empresa, salvo el caso de necesidad efectiva e inaplazable, y previa orden superior.</p>
</li>
	<li>
<p>Aprovecharse del contacto con el usuario del servicio para fines diferentes a los que corresponden al desarrollo del objeto social de la empresa.</p>
</li>
	<li>
<p>Modificar, sin estar autorizado, en cualquier forma, las condiciones y/o características de los productos o servicios ofrecidos o vendidos a los clientes o usuarios.</p>
</li>
</ol>
</li>
	<li>
<p><strong><em>Sobre la permanencia en el puesto de trabajo y cumplimiento de la jornada laboral</em></strong></p>
<ol>
	<li>
<p>El abandono del puesto de trabajo para el cual fue contratado por la empresa.</p>
</li>
	<li>
<p>El incumplimiento injustificado de la jornada de trabajo que acumulado suponga un mínimo de 10 horas al mes.</p>
</li>
	<li>
<p>La tercera falta injustificada de asistencia en un periodo de tres meses.</p>
</li>
	<li>
<p>Las acciones u omisiones dirigidas a evadir los sistemas de control de horarios o a impedir que sean detectados los incumplimientos injustificados de la jornada de trabajo.</p>
</li>
	<li>
<p>La simulación o fraude en la presentación de incapacidades por enfermedad o accidente sea de origen común o laboral.</p>
</li>
	<li>
<p>La suplantación de otro trabajador, alterando registros y controles para tal</p>
</li>
	<li>
<p>No cumplir el horario de trabajo en los términos establecidos en el Reglamento Interno de Trabajo o en el contrato laboral. Siempre que el retardo sea superior a treinta minutos (30).</p>
</li>
	<li>
<p>Faltar al trabajo por más de un (1) día, sin causa justificada.</p>
</li>
</ol>
</li>
	<li><em><strong>Sobre el manejo de la información y actualización de datos personales.</strong></em>
<ol>
	<li>
<p>La publicación o utilización indebida de secretos de la empresa usuaria o Servilabor.</p>
</li>
	<li>
<p>No guardar el debido sigilo respecto a los asuntos que se conozcan por razón del cargo, aun cuando causen perjuicio leve a la empresa usuaria y a Servilabor o se utilice en provecho propio.</p>
</li>
	<li>
<p>El quebrantamiento o violación de secretos de obligada reserva que produzca grave perjuicio para la empresa o la usuaria.</p>
</li>
	<li>
<p>Permitir voluntariamente o por culpa que personas no autorizadas tengan acceso a datos o hechos de conocimiento privativo de la Empresa, de los clientes de ésta o de los usuarios del servicio.</p>
</li>
	<li>
<p>Aprovechar indebidamente, para sí o para terceros, la relación con proveedores, clientes o usuarios del servicio, en su condición de trabajador de la Empresa.</p>
</li>
	<li>
<p>Desatender procedimientos, instrucciones, normas o políticas previstas por la Empresa para el manejo y/o seguridad de procesos, operaciones, software, información y otros asuntos relacionados con el servicio, comunicados al trabajador por la Empresa, bien sea de manera personal o a través de manuales, memorandos, circulares, medios electrónicos o cualquier medio análogo.</p>
</li>
	<li>
<p>Retener injustificadamente en su poder información (en cualquier medio, sea físico o electrónico), archivos o solicitudes que haya recibido para el normal desempeño de sus funciones.</p>
</li>
	<li>
<p>Copiar, divulgar, distribuir o transferir electrónicamente o por cualquier otro medio, programas, archivos, software o manuales de propiedad o bajo licencia de la Empresa, sin previa autorización.</p>
</li>
	<li>
<p>Descompilar, usar ingeniería de reversa, desensamblar, o por cualquier otro medio reducir el software de la Empresa a una forma humanamente perceptible, sin la autorización expresa de ésta.</p>
</li>
	<li>
<p>Arrendar, prestar, vender o crear trabajos derivados basados en el software de la Empresa o en cualquier parte de ellos.</p>
</li>
	<li>
<p>Modificar, adaptar o traducir el software de la Empresa sin el previo consentimiento escrito de ésta.</p>
</li>
	<li>
<p>Prestar o informar las claves de acceso a los programas de sistemas, bien sea las personales o las de otros empleados, que se conozcan en razón del oficio.</p>
</li>
	<li>
<p>Borrar los archivos del sistema que afecten la prestación de servicio.</p>
</li>
	<li>
<p>Afectar el servicio de los equipos de cómputo en ambiente de producción o en ambiente de desarrollo.</p>
</li>
	<li>
<p>Destruir, mutilar o hacer desaparecer documentos de la Empresa, de sus clientes, proveedores y usuarios en general.</p>
</li>
	<li>
<p>Omitir datos o consignar datos inexactos en los informes, relaciones, balances o asientos que presente a los superiores jerárquicos o que sean solicitados por éstos.</p>
</li>
</ol>
</li>
	<li><em><strong>Sobre el manejo de materiales, equipos y maquinaria</strong></em>
<ol>
	<li>
<p>Causar daños graves en los locales, materia o documentos de la empresa usuaria donde se presta el servicio.</p>
</li>
	<li>
<p>Poner en peligro, por actos u omisiones, la seguridad de las personas o de los bienes de la Empresa o de los bienes de terceros confiados a la misma.</p>
</li>
	<li>
<p>Retener, distraer, apoderarse o aprovecharse en forma indebida de dineros, valores u otros bienes que por razón de su oficio en la Empresa tenga que manejar, lleguen a sus manos, o sean elementos de trabajo.</p>
</li>
</ol>
</li>
	<li><em><strong>Sobre la seguridad y Salud en el trabajo</strong></em>
<ol>
	<li>
<p>Consumir en el lugar de trabajo licor, narcóticos o cualquier sustancia que produzca alteraciones en la conducta, o presentarse al trabajo en estado de embriaguez o bajo los efectos de estas mismas sustancias, conforme al C.S.T. y al decreto 1108 de 1994.</p>
</li>
	<li>
<p>La simulación de enfermedad laboral o accidente de trabajo</p>
</li>
	<li>
<p>No cumplir oportunamente las prescripciones que para la seguridad de los locales, los equipos, las operaciones y/o valores de la Empresa o que en ella se manejan, impartan las autoridades de ésta.</p>
</li>
</ol>
</li>
</ol>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>CAPITULO XVIII </b></h4>
<h4 style="text-align: center;"><b>PROCEDIMIENTOS PARA COMPROBACION DE FALTAS Y FORMAS DE APLICACION DE LAS SANCIONES DISCIPLINARIAS</b></h4>
<p><span style="font-weight: 400;"><strong>ARTICULO 52°</strong>: Son sanciones disciplinarias las suspensiones y multas. Los llamados de atención por escrito y amonestaciones verbales son instrumentos del empleador para mantener el orden y disciplina del personal a su cargo, para lo cual requieren de proceso disciplinario previo.</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 53°</strong>: Antes de aplicarse una sanción disciplinaria, el empleador deberá oir al trabajador inculpado de forma verbal o escrita y si éste es sindicalizado deberá estar asistido hasta por dos representantes de la organización sindical a que pertenezca.</span></p>
<p><span style="font-weight: 400;">En todo caso se dejará constancia escrita de los hechos y de la decisión de la empresa de imponer o no, la sanción definitiva (artículo 115, C.S.T.).</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 1</strong>: Se realizara una comunicación formal de la apertura del proceso disciplinario a la persona que se le imputan las conductas posibles de sanción, la cual contendrá fecha, hora y lugar de la diligencia; se hará mención de los hechos objeto de la citación a descargos y se le informara al trabajador la posibilidad de allegar la pruebas que determine necesarias para sustentar su posición.</span></p>
<p><span style="font-weight: 400;">En el caso de negarse a firmar o recibir la correspondiente citación será notificada con la firma de dos testigos a ruego, quienes en presencia del citado dejarán constancia de su renuencia a recibir la citación. En el caso de no encontrarse en turno el trabajador se enviará la respectiva citación por el medio más expedito a la última dirección registrada.</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 2:</strong> Lo establecido en el artículo 54 del presente reglamento interno de trabajo será suspendido en casos de incapacidad médica, permiso sindical, licencia no remunerada, permisos o suspensión del contrato de trabajo.</span></p>
<p><b>ARTICULO 54°: </b><span style="font-weight: 400;">Llegado el día y hora para la realización de la audiencia, se le dará traslado al trabajador de las pruebas, con el propósito que se manifieste de la totalidad de ellas. Se levantará un acta de todo lo narrado y en todo caso se dejara constancia escrita de los hechos y testimonios, si los hubo. La decisión de la empresa de imponer o no sanción será notificada por escrito luego de finalizar la investigación.</span></p>
<p><span style="font-weight: 400;">El término para imponer la sanción será el tiempo prudencial dependiendo de la gravedad y complejidad del caso.</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 1</strong>: Para comprobar las faltas disciplinarias el empleador podrá utilizar los medios probatorios que requiera parar tal fin, como testimonios, documentos, pruebas de alcoholemia y demás pruebas especiales y/o periciales necesarias, de conformidad con el decreto 1108 de 1994 y la sentencia T-183 de1994.</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 55°</strong>: No producirá efecto alguno la sanción disciplinaria impuesta con violación del trámite señalado en los artículos anteriores. (Artículo 115, C.S.T.). </span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 1:</strong> Incurrirán en responsabilidad no sólo los autores de una falta, sino también los superiores o los jefes que la induzcan o toleren, así como aquellos que las encubran.</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 2</strong>: Estas sanciones serán de aplicación sin perjuicio de las responsabilidades civiles o personales que pudieran corresponder. </span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 3:</strong> Todas las faltas y sus correspondientes sanciones se archivarán en el expediente personal del (los) empleado(s) implicado(s).</span></p>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>RECLAMOS</b></h4>
<h4 style="text-align: center;"><b>PERSONAS ANTE QUIENES DEBE PRESENTARSE Y SU TRAMITACION</b></h4>
<p><span style="font-weight: 400;"><strong>ARTICULO 56°</strong> Los reclamos de los trabajadores se harán ante el supervisor de de Gestión Humana, quien los escuchara y resolverá en justicia y equidad.</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 2:</strong> Es necesario indicar que estos reclamos no versaran sobre asuntos relacionados con acoso laboral, dado que esta obligación está reservada para el comité de convivencia laboral de Servilabor.</span></p>
<p><span style="font-weight: 400;"><strong>ARTICULO 57°</strong> Se deja claramente establecido que para efectos de los reclamos a que se refieren los artículos anteriores, el trabajador o trabajadores pueden asesorarse del sindicato respectivo.</span></p>
<p><span style="font-weight: 400;"><strong>PARÁGRAFO 1:</strong> En la empresa NO existen prestaciones adicionales a las legalmente obligatorias.</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<h4 style="text-align: center;"><b>CAPITULO XX </b></h4>
<h4 style="text-align: center;"><b>MECANISMOS DE PREVENCIÓN DEL ACOSO LABORAL Y PROCEDIMIENTO INTERNO DE SOLUCIÓN</b></h4>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 57°</strong>. - Los mecanismos de prevención de las conductas de acoso laboral previstos por la Empresa constituyen actividades tendientes a generar una conciencia colectiva convivente, que promueva el trabajo en condiciones dignas y justas, la armonía entre quienes comparten vida laboral empresarial y el buen ambiente en la empresa y proteja la intimidad, la honra, la salud mental y la libertad de las personas en el trabajo.</span></p>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 58°</strong>.- En desarrollo del propósito a que se refiere el artículo anterior, la empresa ha previsto los siguientes mecanismos.</span></p>
<ol>
	<li><span style="font-weight: 400;"> Información a los trabajadores sobre la Ley 1010 de 2006, resolución 2646 de 2008, resolución 652 de 2012 y 1356 de 2012, que incluya campañas de divulgación preventiva, conversatorios y capacitaciones sobre el contenido de dicha ley, particularmente en relación con las conductas que constituyen acoso laboral, las que no, las circunstancias agravantes, las conductas atenuantes y el tratamiento sancionatorio.</span></li>
	<li><span style="font-weight: 400;"> Espacios para el diálogo, círculos de participación o grupos de similar naturaleza para la evaluación periódica de vida laboral, con el fin de promover coherencia operativa y armonía funcional que faciliten y fomenten el buen trato al interior de la empresa.</span></li>
	<li><span style="font-weight: 400;"> Diseño y aplicación de actividades con la participación de los trabajadores, a fin de:</span>
<ol style="list-style-type: lower-alpha;">
	<li><span style="font-weight: 400;"> Establecer, mediante la construcción conjunta, valores y hábitos que promuevan </span>vida laboral conviviente;</li>
	<li>Formular las recomendaciones constructivas a que hubiere lugar en relación con situaciones empresariales que pudieren afectar el cumplimiento de tales valores y hábitos que promuevan vida laboral conviviente;</li>
	<li><span style="line-height: 1.5;">Examinar conductas específicas que pudieren configurar acoso laboral u otros hostigamientos en la empresa, que afecten la dignidad de las personas, señalando las recomendaciones correspondientes.</span></li>
</ol>
</li>
</ol>
<ol start="4">
	<li><span style="font-weight: 400;"> Las demás actividades que en cualquier tiempo estableciere la empresa para </span>desarrollar el propósito previsto en el artículo anterior.</li>
	<li><span style="font-weight: 400;"> Conformación del Comité de Convivencia laboral, conforme a los lineamientos </span>planteados por las Resoluciones 652 y 1356 de 2012.</li>
</ol>
<p><span style="font-weight: 400;"><strong>ARTÍCULO 59°</strong>.- Para los efectos relacionados con la búsqueda de solución de las conductas de acoso laboral, se establece el siguiente procedimiento interno con el cual se pretende desarrollar las características de confidencialidad, efectividad y naturaleza conciliatoria señaladas por la ley para este procedimiento:</span></p>
<ol>
	<li><span style="font-weight: 400;"> La empresa tendrá un Comité de Convivencia Laboral, integrado en forma bipartita, por un representante de los trabajadores y un representante del empleador o su delegado. Este comité se denominará “Comité de Convivencia Laboral”, reglamentado por la Resolución 652 de 2012.</span></li>
	<li><span style="font-weight: 400;"> El Comité de Convivencia Laboral realizará las siguientes actividades:</span>
<ol style="list-style-type: lower-alpha;">
	<li><span style="font-weight: 400;"> Recibir y dar trámite a las quejas presentadas en las que se describan situaciones que puedan constituir acoso laboral, así como las pruebas que las soportan.</span></li>
	<li><span style="line-height: 1.5;">Examinar de manera confidencial los casos específicos o puntuales en los que se formule queja o reclamo, que pudieran tipificar conductas o circunstancias de acoso laboral, al interior de la entidad pública o empresa privada.</span></li>
	<li><span style="font-weight: 400;">Escuchar a las partes involucradas de manera individual sobre los hechos que </span><span style="line-height: 1.5;">dieron lugar a la queja.</span></li>
	<li><span style="line-height: 1.5;">Formular un plan de mejora concertado entre las partes, para construir, renovar y promover la convivencia laboral, garantizando en todos los casos el principio de la confidencialidad.</span></li>
	<li><span style="line-height: 1.5;">Hacer seguimiento a los compromisos adquiridos por las partes involucradas en la </span>queja, verificando su cumplimiento de acuerdo con lo pactado.</li>
	<li><span style="line-height: 1.5;">En aquellos casos en que no se llegue a un acuerdo entre las partes, no se cumplan las recomendaciones formuladas o la conducta persista, el Comité de Convivencia Laboral, deberá remitir la queja a la Procuraduría General de la </span>Nación, tratándose del sector público. En el sector privado, el Comité informará a la alta dirección de la empresa, cerrará el caso y el trabajador puede presentar la queja ante el inspector de trabajo o demandar ante el juez competente.</li>
	<li><span style="line-height: 1.5;">Presentar a la alta dirección de la entidad pública o la empresa privada las recomendaciones para el desarrollo efectivo de las medidas preventivas y correctivas del acoso laboral, así como el informe anual de resultados de la gestión del comité de convivencia laboral y los informes requeridos por los organismos de control.</span></li>
	<li><span style="line-height: 1.5;">Hacer seguimiento al cumplimiento de las recomendaciones dadas por el Comité de Convivencia a las dependencias de gestión del recurso humano y salud ocupacional de las empresas e instituciones públicas y privadas.</span></li>
	<li><span style="line-height: 1.5;">Elaborar informes trimestrales sobre la gestión del Comité que incluya estadísticas de las quejas, seguimiento de los casos y recomendaciones, los cuales serán presentados a la alta dirección de la entidad pública o empresa privada.</span></li>
	<li><span style="line-height: 1.5;">Adelantar reuniones con el fin de crear un espacio de diálogo entre las partes involucradas, promoviendo compromisos mutuos para llegar a una solución efectiva de las controversias.</span></li>
	<li><span style="line-height: 1.5;">Las demás actividades inherentes o conexas con las funciones anteriores.</span></li>
</ol>
</li>
</ol>
<ol start="3">
	<li><span style="font-weight: 400;"> Este comité se reunirá una vez cada tres meses, designará de su seno un coordinador ante quien podrán presentarse las solicitudes de evaluación de situaciones eventualmente configurantes de acoso laboral con destino al análisis que debe hacer el comité, así como las sugerencias que a través del comité realizaren los miembros de la comunidad empresarial para el mejoramiento de la vida laboral.</span></li>
	<li><span style="font-weight: 400;"> Recibidas las solicitudes para evaluar posibles situaciones de acoso laboral, el comité en la sesión respectiva las examinará, escuchando, si a ello hubiere lugar, a las personas involucradas; construirá con tales personas la recuperación de tejido convivente, si fuere necesario; formulará las recomendaciones que estime indispensables y, en casos especiales, promoverá entre los involucrados compromisos de convivencia.</span></li>
	<li><span style="font-weight: 400;"> Si como resultado de la actuación del comité, éste considerare prudente adoptar medidas disciplinarias, dará traslado de las recomendaciones y sugerencias a los funcionarios o trabajadores competentes de la empresa, para que adelanten los procedimientos que correspondan de acuerdo con lo establecido para estos casos en la ley y en el presente reglamento.</span></li>
	<li><span style="font-weight: 400;"> En todo caso, el procedimiento preventivo interno consagrado en este artículo, no impide o afecta el derecho de quien se considere víctima de acoso laboral para</span></li>
</ol>
<p><span style="font-weight: 400;"></span></p>




	</div>	
	<!-- /row -->
</div>
<!--/container-->
<!--=== End Profile ===-->

